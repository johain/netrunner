# Netrunner

## Project structure

The project has six primary components:

* `android/`: Contains all Android-specific parts of the game. This module has very little code in it, as it mostly has the boilerplate Android stuff, and the launcher. Additionally, it has Android-specific dependencies, though they're managed via Gradle.
* `assets/`: Contains all the game assets, such as graphics. This part contains no code.
* `core/`: Contains all the game logic.
* `desktop/`: Contains all the desktop-specific parts of the game. Similarly to the Android component, it mostly consists of boilerplate desktop stuff, as well as desktop-specific dependencies, also managed via Gradle.
* `model/`: Contains all the data structures. This module is shared between core and server, as both those modules need access to shared data objects to function optimally.
* `server/`: Contains the core code for the server.

Additionally, the following important files and folders exist:

* `etc/`: contains files that are copied to `/etc/` on the Linux-based server. 
* `.gitlab-ci.yml`: Contains the CI definitions, as well as the system used for deploying directly to the server
* `server-bootstrap.sh`: This file is exclusively intended for use on the official server. It installs the required dependencies for the server, and installs the `etc/` folder to the system.

## Building, running, and deploying.

Building components can be done from the command line or your favorite IDE. This will not be demonstrated in detail, as this is fairly straight-forward for the components involved. 

### Optional: running tests

There are several tests, though particularly of the server and model, attempting to validate core parts of the logic.

These can be run from the terminal using `./gradlew test`, or by right-clicking each "test" folder in each module, and clicking "Run tests". While there are facilities to run all tests in an entire project in Android Studio, for reasons we don't understand, it complains about not finding the "COMPILE_JAVA" target, in spite of everything else working fine.

It's also worth noting that certain tests may fail on the first try, particularly among the server tests. This is a consequence of the somewhat non-deterministic behavior of a few of the tests. Attempts have been made to reduce this as much as possible, but it can occasionally happen regardless.

### Server

#### Alternative 1: Official server

Unless otherwise configured, the game connects to a centrally configured server. This server can be used for production use, requiring no further instructions; skip straight to the "Game" header. 

#### Alternative 2: Setting up a debug server (not intended for production use)

**Note:** this must be re-done on each network, as your IP will not remain constant. Redoing it on known networks may also be necessary depending on network settings, but you'll notice that if the game fails to connect.

For debug purposes, it's often more convenient to use a local server. Due to the IP issues this introduces, you're required to specify an IP to connect to.

This is done by creating `assets/local-game.properties`. This file must contain:

```
netrunner.server-addr=xxx.xxx.xxx.xxx
netrunner.server-port=5000
```

The port is 5000 by default in the debug build, so this generally doesn't need to be changed. To find the IP to use, this depends on which configuration you're running


###### Desktop debugging

For desktop debugging, the IP can simply be `127.0.0.1`. The next option works regardless.

###### Android debugging/remote server access

To connect to the server from Android, you have to specify the local IP. How you go about this depends on your OS:

* Linux: `ifconfig`, `ip a`, look for the correct network adapter, or `hostname -I`. Note that some commands may require additional packages. GUI options may exist for different distros, but linking all of them is an exercise in futility. That said, gnome-based DEs shows the IP in the network settings.
* Windows: `ipconfig`, or GUI options. See: https://www.digitalcitizen.life/find-ip-address-windows/ for instructions
* MacOS: `ifconfig`, or GUI options. See: https://www.wikihow.com/Find-Your-IP-Address-on-a-Mac for instructions

###### Running the server

To run a debug version of the server, you can run `./gradlew server:dist` (or `.\gradlew.bat server:dist` on Windows)

### Game

#### Compiling and running locally (recommended for end-users)

Compiling the game locally is best done through Android Studio, and by running the AndroidLauncher in the android module. No steps are required beyond getting the target, and hitting "run" in Android Studio. The build system downloads all the required dependencies. 

#### Building deployment builds

Deployment produces the relevant binaries, and they have to be installed and/or run outside Android Studio, or your favorite editor. These are production packages, and setup is a bit more complicated than just running from within Android Studio (the, by far, recommended approach).

**Note:** prior to deployment, specifically of the desktop and Android apps, `assets/game-local.properties` MUST be removed. This may change in the future if we figure out how to do conditional asset inclusion in different builds.

To deploy the project, the `:dist` target can be used. Example commands:

* Desktop: `./gradlew desktop:dist`
* Server: `./gradlew server:dist`

Android is an exception, and uses a different target: `./gradlew android:assembleRelease`

Note that `./gradlew` has to be replaced with `.\gradlew.bat` on Windows.

See also [LibGDX' own documentation on deployment](https://libgdx.com/wiki/deployment/deploying-your-application)

