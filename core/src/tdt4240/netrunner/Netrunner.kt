package tdt4240.netrunner

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.Viewport
import org.slf4j.LoggerFactory
import tdt4240.netrunner.game.Client
import tdt4240.netrunner.model.game.data.PlayerColor
import tdt4240.netrunner.view.LoadingScreen
import tdt4240.netrunner.view.util.Animation
import tdt4240.netrunner.view.util.TextureRepository
import java.util.Properties
import kotlin.system.exitProcess

class Netrunner : Game() {
    lateinit var skin: Skin
        private set
    lateinit var batch: SpriteBatch
        private set
    lateinit var shapeRenderer: ShapeRenderer
        private set
    lateinit var font: BitmapFont
        private set

    lateinit var uiViewport: Viewport
        private set
    val characterTextures = mutableMapOf<PlayerColor, Animation>()

    override fun create() {
        font = BitmapFont()

        val localProps = Gdx.files.internal("local-game.properties")
        if (localProps.exists()) {
            val p = Properties()
            p.load(localProps.reader())
            for (property in p) {
                System.setProperty(property.key as String, property.value as String)
            }
            logger.info("local-game.properties found and loaded")
        } else {
            logger.info("No local-game.properties found")
        }

        batch = SpriteBatch()
        shapeRenderer = ShapeRenderer()


        skin = Skin(Gdx.files.internal("uiskin.json"))

        uiViewport = FitViewport(MIN_WIDTH, MIN_HEIGHT)

        setScreen(LoadingScreen(this))

        for (color in PlayerColor.values()) {
            characterTextures[color] = Animation(
                    TextureRegion(TextureRepository.runningCharacterBitmaps[color]),
                    8,
                    1.5)

        }
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)

        uiViewport.update(width, height, true)
    }

    override fun dispose() {
        Client.sock.close()

        batch.dispose()
        shapeRenderer.dispose()
        skin.dispose()
        font.dispose()

        TextureRepository.dispose()
        exitProcess(0)
    }

    companion object {
        const val MIN_WIDTH = 1000f;
        const val MIN_HEIGHT = MIN_WIDTH * (9f/16f)

        private val logger = LoggerFactory.getLogger(Netrunner::class.java)
    }

}
