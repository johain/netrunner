package tdt4240.netrunner.game

import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.WebSocket
import org.slf4j.LoggerFactory
import java.net.URI

// This is some top sneaky fixes right here
// Both serverAddr and serverPort are sourced from properties. For now, server-addr is mandatory.
// These are fairly trivial to set from the command line: https://stackoverflow.com/a/5189966/6296561
// Not sure how it's set from the IDE, but this is a solid TODO for later documentation.
//
// TODO: replace the default value for server-addr with the VM IP
object Client {
    private val logger = LoggerFactory.getLogger(Client::class.java)

    // Note: the defaults are set to the "public" matchmaking server.
    // "public" because it requires a VPN or being on campus to access, but in theory, if we had
    // a public server, this would be it.
    private val serverAddr: String = System.getProperty("netrunner.server-addr", "10.212.27.55")
    private val serverPort: Int = System.getProperty("netrunner.server-port", "80").toInt()
    // TODO: figure out the certificate situation. Insecure connections are awful for a large array of reasons.
    // On the other hand, it might not be a priority. It adds complexity (not stonks), and security isn't
    // one of our quality attributes.
    private val tls: Boolean = false

    val sock: Socket = IO.socket(URI((if (!tls) "ws" else "wss") + "://" + serverAddr + ":" + serverPort),
        IO.Options.builder()
                .setTransports(arrayOf(WebSocket.NAME))
                .build())

    /**
     * Reflects the object's internal connection status.
     */
    @Suppress("MemberVisibilityCanBePrivate")
    var sockStatus = ConnInfo()

    init {
        sock.on(Socket.EVENT_CONNECT_ERROR) { it ->
            logger.error("Received error");
            if (it[0] is Exception) {
                logger.error("Error is exception: {}", (it[0] as Exception).stackTraceToString())
                synchronized(this) {
                    sockStatus.connecting = false
                    sockStatus.except = it[0] as Exception
                }
                sock.io().reconnection(false)

            }
        }

        sock.on("connect") {
            logger.info("Connected successfully")
            synchronized(this) {
                sockStatus = ConnInfo()
            }
        }
        sock.on(Socket.EVENT_DISCONNECT) {
            logger.warn("Disconnect received.")
            synchronized(this) {
                sockStatus.disconnected = true
            }
        }
    }

    fun connect() {
        if (sock.connected() || sock.io().isReconnecting) return;
        logger.info("Connecting...")
        sockStatus = ConnInfo(connecting = true)
        sock.connect()
        sock.io().reconnection(true)
    }

}