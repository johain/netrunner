package tdt4240.netrunner.game

import com.badlogic.gdx.Gdx
import io.socket.client.Ack
import org.slf4j.LoggerFactory
import tdt4240.netrunner.Netrunner
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.level.FinishLineComponent
import tdt4240.netrunner.model.game.data.PlayerColor
import tdt4240.netrunner.model.requests.JoinGameRequest
import tdt4240.netrunner.model.response.EntityData
import tdt4240.netrunner.model.response.StatusCode
import tdt4240.netrunner.model.response.StatusResponse
import tdt4240.netrunner.model.util.gsonImpl
import tdt4240.netrunner.view.controllers.CameraController
import tdt4240.netrunner.view.controllers.GroundRenderer
import tdt4240.netrunner.view.controllers.ParallaxRenderer
import tdt4240.netrunner.view.controllers.PlatformRenderer
import tdt4240.netrunner.view.controllers.PlayerRenderer
import tdt4240.netrunner.view.GameScreen
import tdt4240.netrunner.view.controllers.*

/**
 * Client game processor
 *
 * Note that the instance also acts as a sync lock. Anyone invoking any data in this class,
 * except certain methods, are required to do so in a synchronized(gameController) block
 */
class GameController(val game: Netrunner) {
    var gameFound = false
        private set
    var failed = false
        private set
    lateinit var playerUid: String


    val engine = EcsEngine().apply {
        // NOTE: ParallaxController MUST be listed before all other renderers,
        // as it's meant to be the background.
        addController(ParallaxRenderer(this@GameController, this))


        addController(PlayerRenderer(this@GameController, this))
        addController(GroundRenderer(this@GameController, this))
        addController(PlatformRenderer(this@GameController, this))
        addController(PowerUpRenderer(this@GameController, this))
        addController(FinishLineRenderer(this@GameController, this))

        addController(CameraController(this@GameController, this))
    }

    fun joinGame(username: String, playerColor: PlayerColor) {

        // Has to be registered early, or the players event can be missed, which is
        // a bit of a problem.
        // Actually, that's an understatement
        registerGameEvents()
        Client.sock.emit("join-matchmaking", gsonImpl.toJson(
                JoinGameRequest(username, playerColor)
        ), Ack {
            val res = gsonImpl.fromJson(it[0] as String, StatusResponse::class.java)

            synchronized(this) {
                if (res.status != StatusCode.OK) {
                    // TODO: more fine-grained error handling
                    failed = true
                    logger.error("Failed to connect: {}", res.message)
                    deregisterGameEvents()
                } else {
                    gameFound = true
                    playerUid = res.message

                    logger.info("Successfully connected to server. Received UID {}", playerUid)

                }
            }
        })
    }

    fun render() {
        synchronized(engine) {
            engine.tick(Gdx.graphics.deltaTime.toDouble())
            engine.render()
        }
    }

    private fun registerGameEvents() {
        Client.sock.on("entities") {
            val entityData = gsonImpl.fromJson(it[0] as String, EntityData::class.java)
            synchronized(engine) {
                engine.entities = entityData.entities.toMutableList()
            }
        }

        logger.debug("Game events registered")
    }

    fun deregisterGameEvents() {
        // Note: Any registered `sock.on()`s made in this class MUST be deregistered here.
        Client.sock.off("entities")
        Client.sock.off("counter")

        logger.debug("Game events deregistered")
        // Note: this is not an elegant solution. It would be better for the server to emit
        // some type of "game started" event, but it is what it is. We're like two weeks behind at this
        // point. It's fine
        // besides, it doesn't error out, and unless someone is obnoxiously fast, they won't have time
        // to rejoin a game between this being sent and leaving being processed.
        Client.sock.emit("leave-matchmaking")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(GameController::class.java)
    }

}