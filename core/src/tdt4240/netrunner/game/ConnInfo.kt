package tdt4240.netrunner.game

data class ConnInfo(
        /**
         * Control variable; reflects whether there's an ongoing connection attempt or not.
         * This variable is true in the time between calling connect(), and the connection
         * either failing or connecting.
         *
         * sock.io() has `isReconnecting`, but it's unclear whether that applies to all
         * connections or just reconnections.
         */
        var connecting: Boolean = false,
        /**
         * Control variable; reflects whether or not the current connection has DC'd.
         * That may or may not be accompanied by an Exception, though generally will if
         * the connection failed or abruptly ended.
         */
        var disconnected: Boolean = false,
        var except: Exception? = null,
) {
}