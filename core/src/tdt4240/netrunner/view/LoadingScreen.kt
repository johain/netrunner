package tdt4240.netrunner.view

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import org.slf4j.LoggerFactory
import tdt4240.netrunner.Netrunner
import tdt4240.netrunner.game.Client
import tdt4240.netrunner.view.util.StageUtils

class LoadingScreen(val game: Netrunner) : Screen {
    private lateinit var stage: Stage
    private lateinit var table: Table

    private var waiting = true

    override fun show() {
        stage = Stage(game.uiViewport)
        StageUtils.injectBackground(stage)
        table = Table()
        table.skin = game.skin
        table.setFillParent(true)

        stage.addActor(table.apply {
            row()
            add("Connecting to server...", "default-font", Color.WHITE)

        })

        Client.connect()
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        if (waiting) {
            if (!Client.sockStatus.connecting) {
                waiting = false

                if (Client.sock.connected()) {
                    // we've connected; proceed to the menu
                    game.screen = MenuScreen(game)
                    dispose()
                } else if (Client.sockStatus.except != null) {
                    // Failed to connect; notify the user.
                    // The default assumption is heavy load and not a
                    // server error, because to an end user, it doesn't matter.
                    table.clear()
                    table.row()
                    table.add("Failed to connect! :(",
                            "default-font",
                            Color.WHITE)
                    table.row()
                    table.add("Try again later",
                            "default-font",
                            Color.WHITE)
                    table.row()
                    table.add(TextButton("Retry now", game.skin).apply {
                        color = Color.GREEN
                        addListener(object : ClickListener() {
                            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                                logger.info("Retrying server connection...")
                                waiting = true
                                Client.connect()
                            }
                        })

                    }).size(200f, 30f)
                            .pad(20f)
                }
            }
        }

        stage.viewport.apply()
        stage.act(delta)
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun hide() {
    }

    override fun dispose() {
        stage.dispose()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(LoadingScreen::class.java)
    }
}