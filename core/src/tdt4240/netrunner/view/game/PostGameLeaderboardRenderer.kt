package tdt4240.netrunner.view.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Dialog
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Window
import tdt4240.netrunner.Netrunner
import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.game.components.living.PlayerComponent

class PostGameLeaderboardRenderer(private val gController: GameController) {
    private val stage = Stage(gController.game.uiViewport)
    private val table = Table(gController.game.skin)
    init {
        table.setFillParent(true)
        val window = Window("Leaderboard", gController.game.skin)
        window.addActor(table)
        window.width = Netrunner.MIN_WIDTH / 2f
        window.height = Netrunner.MIN_HEIGHT * 2f / 3f
        // Why the actual hell does centering a window require another table?
        // This is so unnecessary
        stage.addActor(Table(gController.game.skin).apply {
            setFillParent(true)
            center()
            add(window).center().width(window.width).height(window.height)

        })

    }

    fun render() {
        val ecs = gController.engine
        val players = ecs.getEntitiesByComponent(PlayerComponent::class.java)
        var showList = false
        val sortedPlayers = players.map {
            it.getComponent(PlayerComponent::class.java)!!
        }.onEach {
            if (it.uid == gController.playerUid && it.finishPosition > 0) {
                showList = true
            }
        }.map { it.finishPosition to it.username }.sortedBy { if (it.first < 0) 1000 else it.first}
        if (showList) {
            table.clear()
            table.apply {
                row().colspan(2)
                add("Leaderboard", "default-font", Color.WHITE).pad(5f)
                for (player in sortedPlayers) {
                    if (player.first == 0) continue
                    row()
                    add(player.second, "default-font", Color.WHITE).pad(5f)
                    add(if(player.first > 0) player.first.toString() else "DNF")
                }
                row()
            }

            gController.game.uiViewport.apply()
            stage.act()
            stage.draw()
        }

    }

    fun dispose() {
        stage.dispose()
    }
}