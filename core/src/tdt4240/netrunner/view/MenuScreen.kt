package tdt4240.netrunner.view

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import tdt4240.netrunner.Netrunner
import tdt4240.netrunner.model.game.data.PlayerColor
import tdt4240.netrunner.view.util.StageUtils


class MenuScreen(private val game: Netrunner) : ScreenAdapter() {

    private lateinit var stage: Stage
    private lateinit var usernameInput: TextField


    //PlayerColor istedet for string
    private lateinit var colorSelectBox: SelectBox<String>

    override fun show() {
        stage = Stage(game.uiViewport)
        StageUtils.injectBackground(stage)

        usernameInput = TextField("", game.skin)
        val labelStyle = Label.LabelStyle(game.skin.getFont("default-font"), Color.WHITE)

        val label = Label("Username:", labelStyle)
        val avatarLabel = Label("Choice of avatar:", labelStyle)

        //PlayerColor
        colorSelectBox = SelectBox<String>(game.skin)
        colorSelectBox.setItems(*PlayerColor.uiRepresentations)

        val playButton = TextButton("Play", game.skin)

        playButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                // TODO: validate username text and player color selection (at least if we add a "none" as a default opt)

                game.screen = GameLoadingScreen(
                        game,
                        usernameInput.text,
                        PlayerColor.reverseString[colorSelectBox.selected]!!
                )


                dispose()
            }
        })

        val table = Table()
        table.pad(20f)
        table.setFillParent(true)
        table.align(Align.left)
        table.row().align(Align.left)
        table.add(label)
        table.row().align(Align.left)
        table.add(usernameInput).size(400f, 100f)
        table.row().align(Align.left)
        table.add(avatarLabel)
        table.row().align(Align.left)
        table.add(colorSelectBox).size(400f, 100f)
        table.row().align(Align.center)
        table.add(playButton)
                .padTop(20f)
                .size(400f, 100f)

        table.row()

        stage.addActor(table)

        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {

        Gdx.gl.glClearColor(0.0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        stage.viewport.apply()
        stage.act(delta)
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
        stage.dispose()
    }
}
