package tdt4240.netrunner.view.controllers

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.level.FinishLineComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent

class FinishLineRenderer(val gController: GameController, engine: EcsEngine) : EcsController(engine) {

    override fun render() {
        for (entity in super.ecs.getEntitiesByComponent(FinishLineComponent::class.java)) {
            val pos = entity.getComponent(PositionComponent::class.java)!!
            val dims = entity.getComponent(DimensionComponent::class.java)!!

            gController.game.apply {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
                shapeRenderer.color = Color.GREEN
                shapeRenderer.rect(pos.pos.x, pos.pos.y, dims.dims.x, dims.dims.y)
                shapeRenderer.end()
            }
        }
    }
}