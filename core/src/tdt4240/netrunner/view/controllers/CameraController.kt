package tdt4240.netrunner.view.controllers

import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.view.GameScreen

class CameraController(val gController: GameController, engine: EcsEngine) : EcsController(engine) {
    override fun tick(delta: Double) {
        val players = super.ecs.getEntitiesByComponent(PlayerComponent::class.java)
        for (player in players) {

            player.getComponent(PlayerComponent::class.java)!!.let {
                player.getComponent(PositionComponent::class.java)!!.let { pos ->
                    player.getComponent(DimensionComponent::class.java)!!.let { dims ->
                        if (it.uid == gController.playerUid) {
                            // If this class is called, the screen is guaranteed to be GameScreen.
                            // If it isn't, undefined behavior.
                            (gController.game.screen as GameScreen).apply {
                                gameViewport.camera.position.x = pos.pos.x + dims.dims.x / 2f
                                gameViewport.camera.position.y = pos.pos.y + dims.dims.y / 2f
                            }
                        }
                    }
                }
            }
        }
    }
}