package tdt4240.netrunner.view.controllers

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.ui.Label
import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent

class PlayerRenderer(val gController: GameController, engine: EcsEngine) : EcsController(engine) {

    override fun render() {
        val labelStyle = Label.LabelStyle()
        labelStyle.font = gController.game.skin.getFont("default-font")
        labelStyle.fontColor = Color.WHITE
        val label = Label(" ", labelStyle)
        label.setFontScale(5F,5F)
        label.setPosition(20f,20f)
        //gController.stage.addActor(label)

        val players = super.ecs.getEntitiesByComponent(PlayerComponent::class.java)
        for (player in players) {
            gController.game.batch.begin()
            player.getComponent(PositionComponent::class.java)!!.also { pos ->
                player.getComponent(PlayerComponent::class.java).also { playerComponent ->
                    player.getComponent(DimensionComponent::class.java)!!.let { dims ->
                        player.getComponent(VelocityComponent::class.java)!!.also{velocity ->
                            val color = playerComponent!!.color
                            gController.game.apply {

                                if (velocity.velocity.x == 0.0.toFloat()){

                                    batch.draw(
                                            // TODO: move to TextureRepository
                                            Texture(playerComponent.color.graphicStand),
                                            pos.pos.x,
                                            pos.pos.y,
                                            PlayerComponent.PLAYER_WIDTH,
                                            PlayerComponent.PLAYER_HEIGHT
                                    )

                                } else {

                                    val texture = characterTextures[color]!!
                                    batch.draw(
                                            texture.getFrame(),
                                            pos.pos.x,
                                            pos.pos.y,
                                            PlayerComponent.PLAYER_WIDTH,
                                            PlayerComponent.PLAYER_HEIGHT
                                    )
                                    texture.update(0.75)
                                }
                                val isCurrentPlayer = playerComponent.uid == gController.playerUid

                                font.color = if (isCurrentPlayer) Color.GREEN else Color.WHITE

                                font.draw(batch, if (isCurrentPlayer) "You" else playerComponent.username,
                                        pos.pos.x + dims.dims.x / 2f,
                                        pos.pos.y + dims.dims.y - 45f)
                            }
                        }
                        // TODO: render username
                    }
                }
                gController.game.batch.end()
            }
        }
    }
}