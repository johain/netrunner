package tdt4240.netrunner.view.controllers

import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.level.PlatformComponent
import tdt4240.netrunner.view.util.TextureRepository

class PlatformRenderer(val gController: GameController, engine: EcsEngine) : EcsController(engine) {
    // This type of texture management is _not_ optimal

    override fun render() {
        val platforms = super.ecs.getEntitiesByComponent(PlatformComponent::class.java)

        val batch = gController.game.batch
        batch.begin()
        for (platform in platforms) {
            platform.getComponent(PositionComponent::class.java)!!.let { pos ->
                platform.getComponent(PlatformComponent::class.java)!!.let { pc ->

                    for (i in 0 until 3) {
                        val texture = TextureRepository.platformTextures[i]
                        val offset = pc.width * i / 3f
                        val length = pc.width / 3f
                        batch.draw(texture,
                                pos.pos.x + offset,
                                pos.pos.y,
                                length,
                                texture.height.toFloat()
                        )
                    }


                }
            }
        }
        batch.end()
    }
}