package tdt4240.netrunner.view.controllers

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.level.GroundComponent

class GroundRenderer(val gController: GameController, engine: EcsEngine) : EcsController(engine) {
    override fun render() {
        gController.game.shapeRenderer.apply {
            begin(ShapeRenderer.ShapeType.Filled)
            color = Color.WHITE

            val ground = super.ecs.getEntitiesByComponent(GroundComponent::class.java)

            for (block in ground) {
                // TODO: explore local culling or possibly GL_CULL_FACE.
                // The latter is significantly easier to implement, but does mean we have to process
                // everything

                block.getComponent(GroundComponent::class.java)!!.let {
                    // This isn't great, but polygons only support line mode :/
                    this.rect(it.startPos.x, 0f,
                        it.endPos.x, it.endPos.y)

                }

            }

            end()
        }
    }
}