package tdt4240.netrunner.view.controllers

import com.badlogic.gdx.graphics.Texture
import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.game.components.powerups.PowerupComponent

class PowerUpRenderer(val gController: GameController, engine: EcsEngine) : EcsController(engine) {
    private val texture = Texture("powerup.png")


    override fun render() {
        gController.game.batch.begin()
        val powerups = super.ecs.getEntitiesByComponent(PowerupComponent::class.java)
        for (powerup in powerups){
            powerup.getComponent(PositionComponent::class.java).also{ pos ->
                if (pos == null){
                    throw RuntimeException("Misconfiguration; this should never throw.")
                }
                    gController.game.apply {
                        batch.draw(texture, pos.pos.x, pos.pos.y, PowerupComponent.WIDTH, PowerupComponent.HEIGHT)
                    }

            }
        }
        gController.game.batch.end()
    }
}