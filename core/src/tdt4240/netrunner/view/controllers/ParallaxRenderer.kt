package tdt4240.netrunner.view.controllers

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import tdt4240.netrunner.Netrunner
import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.util.Vec2f
import tdt4240.netrunner.view.GameScreen
import tdt4240.netrunner.view.util.TextureRepository.parallaxTextures

class ParallaxRenderer(val gController: GameController, engine: EcsEngine) : EcsController(engine) {
    private val batch = SpriteBatch()

    private val layer1Pos = Vec2f(0f, 0f)
    private val layer2Pos = Vec2f(0f, 0f)

    override fun render() {
        // The parallax controller is better off with the UI viewport to keep absolute-ish coords
        // for the fixed background
        gController.game.uiViewport.also {
            it.apply()
            batch.begin()
            batch.projectionMatrix = it.camera.combined
            batch.draw(parallaxTextures[0], 0f, 0f, Netrunner.MIN_WIDTH, Netrunner.MIN_HEIGHT)


            batch.draw(parallaxTextures[1], layer1Pos.x, layer1Pos.y, Netrunner.MIN_WIDTH, Netrunner.MIN_HEIGHT)
            batch.draw(parallaxTextures[1], layer1Pos.x + Netrunner.MIN_WIDTH, layer1Pos.y, Netrunner.MIN_WIDTH, Netrunner.MIN_HEIGHT)

            batch.draw(parallaxTextures[2], layer2Pos.x, layer1Pos.y, Netrunner.MIN_WIDTH, Netrunner.MIN_HEIGHT)
            batch.draw(parallaxTextures[2], layer2Pos.x + Netrunner.MIN_WIDTH, layer1Pos.y, Netrunner.MIN_WIDTH, Netrunner.MIN_HEIGHT)

            batch.end()
        }
        // reset to the normal game viewport
        (gController.game.screen as GameScreen).gameViewport.apply()
    }

    override fun tick(delta: Double) {
        val players = ecs.getEntitiesByComponent(PlayerComponent::class.java)
        for (player in players) {
            if (player.getComponent(PlayerComponent::class.java)!!.uid == gController.playerUid) {
                if (player.getComponent(VelocityComponent::class.java)!!.velocity.x > 0) {
                    // possible future TODO: make the scroll velocity dependent on the player velocity
                    updateAndCheckXPositionExceedingWidth(layer1Pos, SCROLL_SPEED_LAYER_1, delta)
                    updateAndCheckXPositionExceedingWidth(layer2Pos, SCROLL_SPEED_LAYER_2, delta)
                }
                break;
            }
        }
    }

    private fun updateAndCheckXPositionExceedingWidth(pos: Vec2f, scrollSpeed: Float, delta: Double) {
        pos.x -= scrollSpeed * delta.toFloat()
        if (pos.x <= -Netrunner.MIN_WIDTH) {
            pos.x = 0f
        }
    }

    companion object {
        const val SCROLL_SPEED_LAYER_1 = 15f
        const val SCROLL_SPEED_LAYER_2 = 150f
    }
}