package tdt4240.netrunner.view

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import tdt4240.netrunner.Netrunner
import tdt4240.netrunner.game.Client
import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.game.data.PlayerColor
import tdt4240.netrunner.view.util.StageUtils

class GameLoadingScreen(val game: Netrunner, val username: String, val color: PlayerColor) : Screen {
    private lateinit var stage: Stage
    private lateinit var table: Table
    private lateinit var gameController: GameController
    private var hasShownErrorRationale = false
    private val started = System.currentTimeMillis()
    override fun show() {
        stage = Stage(game.uiViewport)
        StageUtils.injectBackground(stage)
        table = Table()

        table.skin = game.skin
        table.setFillParent(true)

        stage.addActor(table.apply {
            row()
            add("Searching for a lobby...", "default-font", Color.WHITE)
        })
        Gdx.input.inputProcessor = stage

        gameController = GameController(game)

        gameController.joinGame(username, color)
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        if ((gameController.failed || Client.sockStatus.except != null) && System.currentTimeMillis() - started >= 2000) {
            if (!hasShownErrorRationale) {
                stage.clear()
                StageUtils.injectBackground(stage)
                stage.addActor(Table(game.skin).apply {
                    setFillParent(true)
                    row().colspan(2)
                    add("Failed to join a room :(", "default-font", Color.WHITE)
                    row()
                    add(TextButton("Return to main menu", game.skin).apply {
                        color = Color.RED
                        addListener(object : ClickListener() {
                            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                                game.screen = MenuScreen(game)
                                dispose()
                            }
                        })
                    })
                            .pad(10f)
                    add(TextButton("Try again", game.skin).apply {
                        color = Color.GREEN
                        addListener(object : ClickListener() {
                            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                                game.screen = GameLoadingScreen(game, username, this@GameLoadingScreen.color)
                                dispose()
                            }
                        })
                    }).width(150f)
                            .pad(10f)
                })
                hasShownErrorRationale = true
            }
        } else if (gameController.gameFound) {
            game.screen = GameScreen(game, gameController)
            dispose()
            return
        }


        stage.viewport.apply()
        stage.act(delta)
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun hide() {
    }

    override fun dispose() {
        stage.dispose()
    }

}