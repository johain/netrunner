package tdt4240.netrunner.view.util

import com.badlogic.gdx.graphics.g2d.TextureRegion

class Animation(val region: TextureRegion, val count: Int, val frameDuration: Double) {
    private val frames = mutableListOf<TextureRegion>()
    private var currentFrameTime = 0.0
    private var frameIndex = 0
    private var timer = 0.0

    init {
        val frameWidth = region.regionWidth / count
        for (i in 0 until count) {
            frames.add(TextureRegion(region, i * frameWidth, 0, frameWidth, region.regionHeight))
        }
    }

    fun update(deltaTime: Double) {
        timer += deltaTime
        while (timer > frameDuration) {
            timer -= frameDuration
            frameIndex++
            if (frameIndex >= count) {
                frameIndex = 0
            }
        }
    }

    fun getFrame(): TextureRegion {
        return frames[frameIndex]
    }
}