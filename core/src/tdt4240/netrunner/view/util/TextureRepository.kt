package tdt4240.netrunner.view.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import org.slf4j.LoggerFactory
import tdt4240.netrunner.model.game.data.PlayerColor

object TextureRepository {
    private val logger = LoggerFactory.getLogger(this::class.java)

    val parallaxTextures = listOf(
            Texture(Gdx.files.internal("bg_0.png")),
            Texture(Gdx.files.internal("bg_1.png")),
            Texture(Gdx.files.internal("bg_2.png")),
    )

    val platformTextures = listOf(
            Texture(Gdx.files.internal("tile-platform-left.png")),
            Texture(Gdx.files.internal("tile-platform-mid.png")),
            Texture(Gdx.files.internal("tile-platform-right.png"))
    )

    val menuBackground = Texture(Gdx.files.internal("bg_menu_v2.png"))

    val runningCharacterBitmaps: MutableMap<PlayerColor, Texture> = mutableMapOf()

    val upButton = Texture(Gdx.files.internal("ui_up.png"))
    val downButton = Texture(Gdx.files.internal("ui_down.png"))
    val exitButton = Texture(Gdx.files.internal("ui_exit.png"))

    init {
        for (color in PlayerColor.values()) {
            runningCharacterBitmaps[color] = Texture(Gdx.files.internal(color.graphicRun))
        }
    }

    fun dispose() {
        logger.warn("Disposing all textures...")

        for (tex in parallaxTextures) tex.dispose()
        for (tex in platformTextures) tex.dispose()
        for (tex in runningCharacterBitmaps) tex.value.dispose()

        upButton.dispose()
        downButton.dispose()

        logger.info("Textures disposed.")
    }
}