package tdt4240.netrunner.view.util

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import tdt4240.netrunner.Netrunner

object StageUtils {
    fun injectBackground(stage: Stage) {
        stage.addActor(Image(TextureRepository.menuBackground).apply {
            x = 0f
            y = 0f
            width = Netrunner.MIN_WIDTH
            height = Netrunner.MIN_HEIGHT
        })
    }
}