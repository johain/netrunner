package tdt4240.netrunner.view

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.viewport.FitViewport
import io.socket.client.Ack
import org.slf4j.LoggerFactory
import tdt4240.netrunner.Netrunner
import tdt4240.netrunner.game.Client
import tdt4240.netrunner.game.GameController
import tdt4240.netrunner.model.response.StatusResponse
import tdt4240.netrunner.model.util.gsonImpl
import tdt4240.netrunner.view.game.PostGameLeaderboardRenderer
import tdt4240.netrunner.view.util.TextureRepository

class GameScreen(private val game: Netrunner, private val controller: GameController) : ScreenAdapter() {
    // I'm 99.9999% sure game.uiViewport means the HUD won't be affected by the game camera and viewport
    private val stage = Stage(game.uiViewport)
    private var cam = OrthographicCamera(Netrunner.MIN_WIDTH, Netrunner.MIN_HEIGHT)
    private val leaderboardRenderer = PostGameLeaderboardRenderer(controller)

    val gameViewport = FitViewport(Netrunner.MIN_WIDTH, Netrunner.MIN_HEIGHT, cam)

    init {
        val buttonStyle = TextButton.TextButtonStyle()
        buttonStyle.font = game.skin.getFont("default-font")
        buttonStyle.downFontColor = Color.RED

        val exitButton = ImageButton(TextureRegionDrawable(TextureRepository.exitButton)) //bruk evt. game.skin
        exitButton.apply {
            width = 70f
            height = 70f
        }
        //if we want an exact position
        //exitButton.setPosition(1000f, 10f)
        // stage.addActor(exitButton)

        val labelStyle = Label.LabelStyle()
        labelStyle.font = game.skin.getFont("default-font")
        labelStyle.fontColor = Color.WHITE
        val label = Label(" ", labelStyle)
        label.setFontScale(5F,5F)
        label.setPosition(20f,20f)
        stage.addActor(label)

        exitButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                game.screen = MenuScreen(game)
                dispose()
            }
        })
        Client.sock.on("counter") { it ->
        if (it.isEmpty()) return@on

        if (it[0] == "0") {
            label.setText("")}
        else {
            label.setText(it[0].toString())
        }}

        val table = Table()
        table.setFillParent(true)
        table.row()
        table.add(exitButton).pad(30f).expand().top().right()
                .width(exitButton.width)
                .height(exitButton.height)
        table.row()
        stage.addActor(table)

        val imageButtonUpStyle = ImageButtonStyle()
        val drawableUp = TextureRegionDrawable(TextureRepository.upButton)
        imageButtonUpStyle.imageUp = drawableUp
        imageButtonUpStyle.imageDown = drawableUp
        val buttonUp = ImageButton(imageButtonUpStyle)
        buttonUp.height = BUTTON_DIMS
        buttonUp.width = BUTTON_DIMS
        buttonUp.setPosition(Netrunner.MIN_WIDTH - buttonUp.width - 20f, 100f)
        buttonUp.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                Client.sock.emit("jump", Ack{
                    val res = gsonImpl.fromJson(it[0] as String, StatusResponse::class.java)
                    println(res.message)
                })
                logger.info("Button UP clicked!")
            }
        })
        stage.addActor(buttonUp)

        val imageButtonDownStyle = ImageButtonStyle()
        val drawableDown = TextureRegionDrawable(TextureRepository.downButton)
        imageButtonDownStyle.imageUp = drawableDown
        imageButtonDownStyle.imageDown = drawableDown
        val buttonDown = ImageButton(imageButtonDownStyle)
        buttonDown.height = BUTTON_DIMS
        buttonDown.width = BUTTON_DIMS
        buttonDown.setPosition(Netrunner.MIN_WIDTH - buttonUp.width - 20f, 20f)
        buttonDown.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                // TODO: Send to server?
                logger.info("Button DOWN clicked!")
            }
        })
        stage.addActor(buttonDown)
        cam.position.y = 100f
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)


        gameViewport.camera.update()
        // I don't understand libgdx' viewports. Why does this need to be applied when the projection
        // matrices also have ot be manually applied?
        gameViewport.apply()
        game.batch.projectionMatrix = gameViewport.camera.combined
        game.shapeRenderer.projectionMatrix = gameViewport.camera.combined
        // The game has to be rendered prior to the UI for display order reasons
        controller.render()

        // Also, why does the viewport have to be manually applied for stages??
        // This could've been moved into stage.draw() (not our code though, so nothing we can do about that)
        // stage.draw() already applies the projection matrix to its own batch, so there isn't any reason
        // it can't apply the viewport.
        // Granted, it does have its own batch, but whatever.
        stage.viewport.apply()
        stage.act(delta)
        stage.draw()

        leaderboardRenderer.render()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        gameViewport.update(width, height)
    }

    override fun dispose() {
        stage.dispose()
        controller.deregisterGameEvents()
        leaderboardRenderer.dispose()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(Netrunner::class.java)

        const val BUTTON_DIMS = 100f
    }

}
