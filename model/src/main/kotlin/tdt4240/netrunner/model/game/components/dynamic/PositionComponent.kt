package tdt4240.netrunner.model.game.components.dynamic

import tdt4240.netrunner.model.game.components.Component
import tdt4240.netrunner.model.util.Vec2f

data class PositionComponent(var pos: Vec2f) : Component {
    override val componentName: String = this::class.java.name

}