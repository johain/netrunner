package tdt4240.netrunner.model.game.components.dynamic

import tdt4240.netrunner.model.game.components.Component

data class VelocityModifier(
        // Required
        val speedModifier: Float,
        // Optional support data object
        val id: Long = 0,
        // Optional support data object
        val timestamp: Long = 0,
) {

}

class VelocityModifierComponent(
        /**
         * <ModifierID, ModifierData>
         * Note that the ModifierData consists of a List of VelocityModifiers, a support class defining
         * common variables.
         * Note that the usage of the variables is up to the controller. The exact meanings of the variables
         * may vary by entity, but this aims to provide a unified interface.
         *
         * Invalid uses are not validated by the component, as this would be controller behavior extending
         * far outside the scope of what can be reasoned to be within the scope of a data object.
         *
         *
         */
        val mods: MutableMap<String, MutableList<VelocityModifier>> = mutableMapOf()
) : Component {
    override val componentName: String = this::class.java.name

}