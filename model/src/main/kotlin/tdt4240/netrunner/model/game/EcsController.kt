package tdt4240.netrunner.model.game

import tdt4240.netrunner.model.game.components.Component

/**
 * Controller for the ECS system, for a type of component.
 *
 * Implementations must override tick() or render() at least.
 */
open class EcsController (protected val ecs: EcsEngine) {

    open fun tick(delta: Double) {}


    open fun render() {}
}