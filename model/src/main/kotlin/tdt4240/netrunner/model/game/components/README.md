# Note

Because of Limitations:tm:, all components must override the componentName field (this is also enforced), but it has to be done using this line of code:
```kotlin
    override val componentName: String = this::class.java.name
```
Just copy it into the class; the deserialisation class takes care of the rest. 
