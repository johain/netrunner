package tdt4240.netrunner.model.game.factories

import tdt4240.netrunner.model.game.Entity
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.level.PlatformComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent
import tdt4240.netrunner.model.util.Vec2f

class PlatformFactory {
    private lateinit var left: Vec2f
    private var width: Int = 0

    fun withLeftPos(left: Vec2f) = also {
        this.left = left
    }

    fun withWidth(width: Int) = also {
        this.width = width
    }

    fun build() = Entity(mutableSetOf(
            PositionComponent(left),
            PlatformComponent(width),
            DimensionComponent(Vec2f(width.toFloat(), PLATFORM_HEIGHT))
    ))

    companion object {
        const val PLATFORM_HEIGHT = 40f
    }

}