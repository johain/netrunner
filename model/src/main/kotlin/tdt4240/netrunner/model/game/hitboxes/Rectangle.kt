package tdt4240.netrunner.model.game.hitboxes

import tdt4240.netrunner.model.util.Vec2f

class Rectangle(val pos: Vec2f, val dims: Vec2f) : Hitbox {
    override fun overlaps(other: Rectangle)
            = !(pos.x + dims.x < other.pos.x
            || pos.x > other.pos.x + other.dims.x
            || pos.y + dims.y < other.pos.y
            || pos.y > other.pos.y + other.dims.y)



}