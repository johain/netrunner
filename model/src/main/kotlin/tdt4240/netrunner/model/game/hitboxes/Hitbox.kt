package tdt4240.netrunner.model.game.hitboxes

interface Hitbox {
    fun overlaps(other: Rectangle): Boolean
}