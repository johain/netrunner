package tdt4240.netrunner.model.game.components.living

import tdt4240.netrunner.model.game.components.Component
import tdt4240.netrunner.model.game.data.PlayerColor


class PlayerComponent(val username: String,
                      val color: PlayerColor,
                      val uid: String,
                      /**
                       * The position in which the player finished.
                       *
                       * > 0: the position
                       * 0: Not done, or DNF failed to reach the goal in time
                       * <0: DNF; exclusively game abandoned
                       */
                      var finishPosition: Int = 0
) : Component {
    override val componentName: String = this::class.java.name

    companion object {
        const val SCALE_FACTOR = 0.8f
        const val PLAYER_WIDTH = 200f * SCALE_FACTOR
        const val PLAYER_HEIGHT = 250f * SCALE_FACTOR
    }
}