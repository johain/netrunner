package tdt4240.netrunner.model.game.factories

import tdt4240.netrunner.model.game.Entity
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent
import tdt4240.netrunner.model.game.components.living.LivingComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.game.components.powerups.PowerupComponent
import tdt4240.netrunner.model.game.data.PlayerColor
import tdt4240.netrunner.model.util.Vec2f
import java.lang.reflect.Modifier

class PowerupFactory {
    lateinit var pos:Vec2f
    var speedModifier:Float = 0f

    fun withPos(pos:Vec2f) = this.also {
        this.pos = pos
    }

    fun withSpeedModifier(speedModifier:Float) = this.also {
        this.speedModifier = speedModifier
    }


    fun build() : Entity {
        return Entity(hashSetOf(
            PositionComponent(pos),
            PowerupComponent(speedModifier),
            DimensionComponent(Vec2f(PowerupComponent.WIDTH, PowerupComponent.HEIGHT))
        ))
    }
}