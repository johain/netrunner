package tdt4240.netrunner.model.game.components.powerups

import tdt4240.netrunner.model.game.components.Component
import java.lang.reflect.Modifier

class PowerupComponent (val speedModifier: Float, var consumed: Boolean = false) : Component {
    override val componentName: String = this::class.java.name

    companion object{
        const val WIDTH = 60f
        const val HEIGHT = 60f
    }
}