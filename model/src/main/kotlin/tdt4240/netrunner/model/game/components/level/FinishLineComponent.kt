package tdt4240.netrunner.model.game.components.level

import tdt4240.netrunner.model.game.components.Component

class FinishLineComponent() : Component {
    override val componentName: String = this::class.java.name
}