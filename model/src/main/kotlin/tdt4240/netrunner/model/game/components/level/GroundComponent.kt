package tdt4240.netrunner.model.game.components.level

import tdt4240.netrunner.model.game.components.Component
import tdt4240.netrunner.model.util.Vec2f

/**
 * ECS component defining the ground.
 *
 * Note that {@param startPos} and {@param endPos} define the top-left and top-right corners
 * respectively. The bottom left and right corners are implicitly at height 0, at the x coordinate
 * defined by the matching position.
 */
data class GroundComponent(val startPos: Vec2f, val endPos: Vec2f) : Component {
    init {
        if (startPos > endPos) {
            // Enforce order
            throw IllegalArgumentException("start has to be before end")
        } else if (startPos == endPos) {
            // Enforce min size
            throw IllegalArgumentException("Cannot have a zero-width ground component")
        } else if (endPos.x - startPos.x > MAX_WIDTH) {
            // Enforce max size
            throw IllegalArgumentException("Length exceeds max component width")
        }
    }
    // Deserialisation necessity
    override val componentName: String = this::class.java.name

    fun yAt(x: Float) : Float {
        val slope = endPos.y - startPos.y
        val slopePerX = slope / (endPos.x - startPos.x)

        val relativeX = startPos.x - x

        return startPos.y + slopePerX * relativeX
    }
    companion object {
        const val MAX_WIDTH = 100f
    }
}