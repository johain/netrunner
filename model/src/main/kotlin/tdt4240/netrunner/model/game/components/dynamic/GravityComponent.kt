package tdt4240.netrunner.model.game.components.dynamic

import tdt4240.netrunner.model.game.components.Component
import tdt4240.netrunner.model.util.Vec2f


class GravityComponent(val gravSpeed: Vec2f = Vec2f(0f, GRAVITY)) : Component {
    override val componentName: String = this::class.java.name



    companion object {
        const val GRAVITY = -2000.0f
        //TODO:
    }
}