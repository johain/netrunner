package tdt4240.netrunner.model.game

import tdt4240.netrunner.model.game.components.Component

class Entity(initComponents: MutableSet<Component>) {
    val components = initComponents.associateBy {
        it::class.java.name
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Component> getComponent(clazz: Class<T>): T? {
        return components[clazz.name] as T?
    }


}