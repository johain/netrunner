package tdt4240.netrunner.model.game.factories

import tdt4240.netrunner.model.game.Entity
import tdt4240.netrunner.model.game.components.dynamic.GravityComponent
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityModifierComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent
import tdt4240.netrunner.model.game.components.living.LivingComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.game.data.PlayerColor
import tdt4240.netrunner.model.util.Vec2f

class PlayerFactory {
    // Leveraging lateinit properties makes it easier to validate completion of mandatory properties
    private lateinit var username: String
    private lateinit var initPosition: Vec2f
    private lateinit var uid: String

    private var color: PlayerColor = PlayerColor.BLUE

    fun withPosition(pos: Vec2f) = this.also {
        initPosition = pos
    }
    fun withUsername(username: String) = this.also {
        this.username = username
    }
    fun withColor(color: PlayerColor) = this.also {
        this.color = color
    }

    fun withUid(uid: String) = this.also {
        this.uid = uid
    }

    fun build() : Entity {
        require(::username.isInitialized) { "Username is not initialized" }

        return Entity(hashSetOf(
                PositionComponent(initPosition),
                VelocityComponent(Vec2f()),
                PlayerComponent(username, color, uid),
                LivingComponent(100, DEFAULT_MAX_PLAYER_SPEED),
                DimensionComponent(Vec2f(PlayerComponent.PLAYER_WIDTH, PlayerComponent.PLAYER_HEIGHT)),
                GravityComponent(),
                VelocityModifierComponent()
        ))
    }

    companion object {
        /**
         * TODO: The value is currently semi-arbitrary; update to something more relevant
         */
        const val DEFAULT_MAX_PLAYER_SPEED = 100.0f
    }
}