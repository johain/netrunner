package tdt4240.netrunner.model.game.components.living

import tdt4240.netrunner.model.game.components.Component
import tdt4240.netrunner.model.util.Vec2f

class DimensionComponent(val dims: Vec2f) : Component {
    override val componentName: String = this::class.java.name
}