package tdt4240.netrunner.model.game.components.living

import tdt4240.netrunner.model.game.components.Component

data class LivingComponent(var health: Int, var maxSpeed: Float) : Component {
    override val componentName: String = this::class.java.name

    val isAlive: Boolean
        get() = health > 0
}