package tdt4240.netrunner.model.game

import tdt4240.netrunner.model.game.components.Component

class EcsEngine {
    // Ashley (LibGDX's own ECS library) also stores entities in a single list.
    // This isn't fantastic, but with how limited java is (C++ templates would be far better for this),
    // there aren't all that many great ways to store entities
    var entities = mutableListOf<Entity>()
    private val controllers = mutableListOf<EcsController>()

    fun addEntity(e: Entity) {
        entities.add(e)
    }

    fun tick(delta: Double) {
        for (controller in controllers) {
            controller.tick(delta)
        }
    }

    fun render() {
        for (controller in controllers) {
            controller.render()
        }
    }

    fun <T : Component> getEntitiesByComponent(clazz: Class<T>): List<Entity> {
        return entities.filter {
            it.getComponent(clazz) != null
        }
    }

    fun addController(controller: EcsController) {
        controllers.add(controller)
    }

}