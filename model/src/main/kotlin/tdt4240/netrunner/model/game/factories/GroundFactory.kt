package tdt4240.netrunner.model.game.factories

import tdt4240.netrunner.model.game.Entity
import tdt4240.netrunner.model.game.components.level.GroundComponent
import tdt4240.netrunner.model.util.Vec2f

class GroundFactory {
    private lateinit var startPos: Vec2f
    private lateinit var endPos: Vec2f

    fun withStartPos(startPos: Vec2f) : GroundFactory {
        this.startPos = startPos
        return this
    }

    fun withEndPos(endPos: Vec2f): GroundFactory {
        this.endPos = endPos
        return this
    }

    fun build() : Entity {
        return Entity(mutableSetOf(
                GroundComponent(startPos, endPos)
        ))
    }
}