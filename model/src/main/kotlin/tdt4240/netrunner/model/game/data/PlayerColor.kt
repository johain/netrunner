package tdt4240.netrunner.model.game.data

enum class PlayerColor(val graphicStand: String, val  graphicRun: String, val uiRepresentation: String) {
    GREEN("char_green_stand.png", "char_green_run.png","Green"),
    BLUE("char_blue_stand.png","char_blue_run.png","Blue"),
    RED("char_red_stand.png","char_red_run.png","Red"),
    YELLOW("char_yellow_stand.png","char_yellow_run.png", "Yellow");

    companion object {
        val uiRepresentations by lazy {
            values().map { it.uiRepresentation}.toTypedArray()
        }
        val reverseString by lazy {
            values().associateBy { it.uiRepresentation }
        }
    }
}