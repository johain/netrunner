package tdt4240.netrunner.model.game.components

import java.sql.BatchUpdateException

interface Component {
    val componentName: String
}