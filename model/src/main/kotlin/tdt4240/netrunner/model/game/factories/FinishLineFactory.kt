package tdt4240.netrunner.model.game.factories

import tdt4240.netrunner.model.game.Entity
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.level.FinishLineComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.util.Vec2f

class FinishLineFactory {
    private lateinit var pos: Vec2f

    fun withPos(pos: Vec2f) = also {
        this.pos = pos
    }

    fun build() = Entity(mutableSetOf(
            PositionComponent(pos),
            DimensionComponent(Vec2f(20f, PlayerComponent.PLAYER_HEIGHT * 10)),
            FinishLineComponent(),
    ))
}