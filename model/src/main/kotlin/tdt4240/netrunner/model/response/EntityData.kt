package tdt4240.netrunner.model.response

import tdt4240.netrunner.model.game.Entity

data class EntityData(var entities: List<Entity>) {
    operator fun get(idx: Int) = entities[idx]
}