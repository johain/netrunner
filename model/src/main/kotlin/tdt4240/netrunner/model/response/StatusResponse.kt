package tdt4240.netrunner.model.response

/**
 * Used as a semi-generic response for endpoints without more detailed data structures, such as
 * the game, that outputs complex player objects or updates.
 */
data class StatusResponse(val status: StatusCode, val message: String = "")

enum class StatusCode {
    /**
     * Indicates a completed request
     */
    OK,

    /**
     * Indicates a request that failed. A message should always accompany this.
     * An example of its use is when returning from an endpoint experienced an internal error, such
     * as trying to matchmake a player in a game that just filled up.
     *
     * In essence, this is called when nothing is wrong with the input, but the user just has to
     * try again. The client can use this as an indicator to automatically retry.
     */
    FAIL,

    /**
     * Indicates a general endpoint error caused by input:
     * examples include incorrect credentials or other invalid data, unexpected errors concealed as expected, ...
     */
    DATA_ERROR,

    /**
     * Developer error: returned when the developer forgot supplying enough arguments.
     */
    INSUFFICIENT_ARGUMENTS
}
