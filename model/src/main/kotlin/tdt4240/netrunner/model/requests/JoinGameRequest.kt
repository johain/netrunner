package tdt4240.netrunner.model.requests

import tdt4240.netrunner.model.game.data.PlayerColor

data class JoinGameRequest(val username: String, val color: PlayerColor)
