package tdt4240.netrunner.model.requests

/**
 * Data wrapper for authentication requests.
 *
 * This is intended used for both signup and login, as we won't be storing or using any more data for
 * login.
 * We could add emails to the signup requests at a later point in time, but it's not currently worth
 * the development overhead to bother with email on a higher "if this becomes big there has to be measures
 * to avoid spam"-scheme of things.
 */
data class AuthRequest(val username: String, val password: String)
