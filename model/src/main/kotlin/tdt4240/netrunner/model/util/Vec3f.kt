package tdt4240.netrunner.model.util

data class Vec3f(var x: Float, var y: Float, var z: Float) {
    constructor() : this(0f, 0f, 0f) {}
    // TODO: operators
}