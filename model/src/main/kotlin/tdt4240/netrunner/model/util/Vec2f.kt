package tdt4240.netrunner.model.util

import kotlin.math.pow
import kotlin.math.sqrt

data class Vec2f(var x: Float, var y: Float) {
    constructor() : this(0f, 0f) {}

    operator fun compareTo(other: Vec2f): Int = when {
        x > other.x -> 1
        x < other.x -> -1
        y > other.y -> 1
        y < other.y -> -1
        else -> 0
    }

    operator fun times(constant: Float) : Vec2f = Vec2f(x * constant, y * constant)
    operator fun times(constant: Double) : Vec2f = Vec2f(x * constant.toFloat(), y * constant.toFloat())

    operator fun minus(other: Vec2f): Vec2f
        = Vec2f(x - other.x, y - other.y)
    operator fun plus(other: Vec2f): Vec2f
        = Vec2f(x + other.x, y + other.y)

    operator fun unaryMinus() = Vec2f(-x, -y)
    operator fun unaryPlus() = Vec2f(x, y)

    operator fun plusAssign(other: Vec2f) {
        x += other.x
        y += other.y
    }

    operator fun minusAssign(other: Vec2f) {
        x -= other.x
        y -= other.y
    }

    fun len(origin: Vec2f = Vec2f(0f, 0f))
        = sqrt((x - origin.x).pow(2) + (y - origin.y).pow(2))
}