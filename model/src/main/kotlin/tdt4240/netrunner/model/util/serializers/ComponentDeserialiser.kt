package tdt4240.netrunner.model.util.serializers

import com.google.gson.*
import tdt4240.netrunner.model.game.components.Component
import java.lang.reflect.Type

/**
 * Utility class for deserialising the Components used in Entities.
 *
 * Gson doesn't like  deserialising interfaces, because the information required to deserialise them
 * is discarded.
 */
class ComponentDeserialiser<T : Component>() : JsonDeserializer<T> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): T {
        val obj = json.asJsonObject
        val prim = obj.get("componentName") as JsonPrimitive
        val className = prim.asString
        // Hack: resolve the component name by string. There's several different components, and no
        // other good way to resolve them.
        val c = Class.forName(className)

        return context.deserialize(obj, c)

   }

}