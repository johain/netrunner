package tdt4240.netrunner.model.util

data class Vec4f(var x: Float, var y: Float, var z: Float, var w: Float) {
    constructor() : this(0f, 0f, 0f, 0f) {}
    // TODO: operators
}