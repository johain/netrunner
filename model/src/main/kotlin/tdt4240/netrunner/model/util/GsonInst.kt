package tdt4240.netrunner.model.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import tdt4240.netrunner.model.game.components.Component
import tdt4240.netrunner.model.util.serializers.ComponentDeserialiser

/**
 * Shared GSON implementation. Includes some additional serialisers where necessary
 *
 */
val gsonImpl: Gson = GsonBuilder()
        .registerTypeAdapter(Component::class.java, ComponentDeserialiser<Component>())
        .create()