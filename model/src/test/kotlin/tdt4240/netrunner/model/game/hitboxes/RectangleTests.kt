package tdt4240.netrunner.model.game.hitboxes

import tdt4240.netrunner.model.util.Vec2f
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class RectangleTests {

    @Test
    fun validateOverlap() {
        val bb = Rectangle(Vec2f(10f, 10f), Vec2f(10f, 10f))
        assertTrue(bb.overlaps(Rectangle(Vec2f(10f, 10f), Vec2f(1f, 1f))))
        assertTrue(bb.overlaps(Rectangle(Vec2f(8f, 8f), Vec2f(2f, 2f))))
        assertTrue(bb.overlaps(Rectangle(Vec2f(19f, 19f), Vec2f(10f, 10f))))
        assertTrue(bb.overlaps(Rectangle(Vec2f(-100f, 0f), Vec2f(1000f, 1000f))))
    }

    @Test
    fun validateDoesNotOverlap() {
        val bb = Rectangle(Vec2f(10f, 10f), Vec2f(10f, 10f))
        assertFalse(bb.overlaps(Rectangle(Vec2f(8f, 8f), Vec2f(1f, 1f))))
        assertFalse(bb.overlaps(Rectangle(Vec2f(7f, 7f), Vec2f(2f, 2f))))
        assertFalse(bb.overlaps(Rectangle(Vec2f(21f, 21f), Vec2f(10f, 10f))))
        assertFalse(bb.overlaps(Rectangle(Vec2f(-100f, 0f), Vec2f(10f, 10f))))
    }
}