package tdt4240.netrunner.model.util

import kotlin.test.*
import kotlin.math.*

class VecTests {
    @Test
    fun testVec2RelativeComparisonOperators() {
        val v1 = Vec2f(0f, 0f)
        val v2 = Vec2f(1f, 0f)
        val v3 = Vec2f(0f, 1f)
        val v4 = Vec2f(-1f, -1f)

        assertTrue(v2 > v1)
        assertTrue(v2 > v3)
        assertTrue(v3 > v4)
        assertTrue(v2 > v3)

        assertFalse(v1 > v2)
        assertFalse(v3 > v2)
        assertFalse(v4 > v3)
        assertFalse(v3 > v2)

        assertTrue(v1 < v2)
        assertTrue(v3 < v2)
        assertTrue(v4 < v3)
        assertTrue(v3 < v2)

        val v5 = Vec2f(0.5f, 0.5f)
        assertTrue(v5 > v1)
        assertTrue(v1 < v5)
        assertFalse(v1 > v5)
        assertFalse(v5 < v1)

        assertFalse(v1 < v1)
        assertFalse(v1 > v1)
        assertTrue(v1 >= v1)
        assertTrue(v1 <= v1)
    }

    @Test
    fun testVec2PlusMinusAssign() {
        val v1 = Vec2f(0f, 0f)
        val v2 = Vec2f(4f, 1f)
        val v3 = Vec2f(-1f, 3f)
        assertEquals(v2, v1 + v2)
        assertEquals(Vec2f(3f, 4f), v2 + v3)
        assertEquals(v3, v3 + v1)

        assertEquals(v3, v3 - v1)
        assertEquals(v2, v2 - v1)
        assertEquals(-v2, v1 - v2)

        val v2Cache = v2.copy()
        val v3Cache = v3.copy()
        v2 += v3
        assertEquals(v3Cache, v3)
        assertNotEquals(v2Cache, v2)

        assertEquals(Vec2f(3f, 4f), v2)
        v2 -=  v3
        assertEquals(v2Cache, v2)
        assertEquals(v3Cache, v3)

    }

    @Test
    fun testVecLength() {
        val v1 = Vec2f(1f, 1f)
        val v2 = Vec2f(2f, 2f)

        val v3 = Vec2f(4f, 1f)

        assertEquals(sqrt(2f), v1.len())
        assertEquals(2f * sqrt(2f), v2.len())
        assertEquals(sqrt(2f), v2.len(v1))
        assertEquals(sqrt(2f), v1.len(v2))

        assertEquals(3f, v3.len(v1))
        assertEquals(3f, v1.len(v3))


    }
}