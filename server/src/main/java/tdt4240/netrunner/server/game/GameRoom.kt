package tdt4240.netrunner.server.game

import io.socket.socketio.server.SocketIoServer
import org.slf4j.LoggerFactory
import tdt4240.netrunner.game.WorldGen
import tdt4240.netrunner.game.controllers.GravityController
import tdt4240.netrunner.game.controllers.MovementController
import tdt4240.netrunner.game.controllers.WinController
import tdt4240.netrunner.game.controllers.powerupSpeedController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.Entity
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.game.factories.PlayerFactory
import tdt4240.netrunner.model.requests.JoinGameRequest
import tdt4240.netrunner.model.response.EntityData
import tdt4240.netrunner.model.util.Vec2f
import tdt4240.netrunner.model.util.gsonImpl
import tdt4240.netrunner.server.controllers.GameServerEndpointController
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap

class GameRoom(val socketRoomID: String, val server: SocketIoServer, val controller: GameServerEndpointController){
    private val logger = LoggerFactory.getLogger("room-$socketRoomID")
    private val gen = WorldGen(this)
    private val ns = server.namespace("/")

    val createdAt = System.currentTimeMillis()
    val players = ConcurrentHashMap<String, Entity>()
    val ecs = EcsEngine().also {
        it.entities.addAll(gen.genWorld(STANDARD_FIELD_LENGTH))
    }
    var started = false
        private set
    var done = false
        private set

    /**
     * Test utility variable.
     *
     * DO NOT USE OUTSIDE TESTS!
     */
    var forceStart = false

    init {
        ecs.addController(MovementController(ecs))
        ecs.addController(GravityController(ecs))
        ecs.addController(powerupSpeedController(ecs))
        ecs.addController(WinController(ecs))

        logger.info("New room created. ID: ${socketRoomID}")
    }

    val thread = Thread(::tick).also {
        if (controller.running) {
            it.start()
        }
    }

    private fun tick() {
        var prevTime = System.currentTimeMillis()
        var doneTime = Long.MAX_VALUE
        while (controller.running && !done) {
            if (!started) {
                if (System.currentTimeMillis() - createdAt >= MAX_WAIT_TIME * 1000 && players.size >= MIN_PLAYERS
                        || players.size == MAX_PLAYERS || forceStart) {
                    logger.info("Play threshold reached. Starting game")

                    started = true
                    // TODO: timer

                    // TODO: broadcast time remaining every second or so?
                    // How many players have joined is automatically broadcast whenever someone joins or leaves,
                    // so that doesn't need to be handled here.
                    prevTime = System.currentTimeMillis()
                    ecs.entities.addAll(players.values.toMutableList())

                    controller.notifyStart(this)

                    for (i in 3 downTo 0) {
                        Thread.sleep(1000)
                        server.namespace("/").broadcast(socketRoomID, "counter", gsonImpl.toJson(i))
                    }
                    server.namespace("/").broadcast(socketRoomID, "game-started")

                    for((_, player) in players)
                        player.getComponent(VelocityComponent::class.java)!!.velocity.x = PLAYER_RUN_VELOCITY

                } else if (System.currentTimeMillis() - createdAt >= MAX_WAIT_TIME * 1000 && players.size == 0) {
                    synchronized(this) {
                        done = true
                    }
                    controller.notifyDead(this)
                    return
                } else {
                    Thread.sleep(1000)
                    continue
                }
            }

            val delta = ((System.currentTimeMillis() - prevTime).toDouble())/1000.0

            ecs.tick(delta)

            // Preliminary code: broadcasts the players to the server. Game ticks go above this.
            try {
                emitPlayers()
            } catch (e: ConcurrentModificationException) {
                // Pass: will have to be fine on the next tick instead.
            }
            prevTime = System.currentTimeMillis()
            Thread.sleep((1000.0 / 60.0).toLong())
            // Checks for game completion
            // The game is considered done if all players except one has either crossed
            // the finish line, or has left (DNF).
            // The last player to finish will, obviously and irrecoverably, be in last place, and
            // consequently not count.
            //
            // We should probably have a mechanic for adding a timer after the first completion as well,
            // so the game rooms are forced to finish after An Amount Of Time:tm:
            if (ecs.getEntitiesByComponent(PlayerComponent::class.java)
                            .map { it.getComponent(PlayerComponent::class.java)!!.finishPosition }
                            .count { it != 0 } >= players.size - 1) {
                // Give 10 seconds for last minute processing
                if (doneTime == Long.MAX_VALUE) {
                    doneTime = System.currentTimeMillis()
                } else if (System.currentTimeMillis() - doneTime > 10_000) {
                    break
                }
            }
        }
        // TODO: emit leaderboard event, if we have the time to implement that FR

        controller.notifyDone(this)
    }

    fun emitPlayers() {
        for (i in 0..5) {
            try {
                // we should probably separate the world from the player entities, just for
                // convenience (and performance, sending a few hundred entities over and over
                // isn't great. It works fine, but it's a waste of data).
                ns.broadcast(socketRoomID,
                        "entities",
                        gsonImpl.toJson(
                                EntityData(if (started) ecs.entities else players.values.toList())
                        )
                )
                return
            } catch (e: ConcurrentModificationException) {
                logger.error("Failed to broadcast: ConcurrentModificationException")
            }
        }
    }

    /**
     * This function kills the room. DO NOT USE to end the game unless the server is being killed
     */
    fun kill() {
        thread.join()
    }

    fun join(socketID: String, req: JoinGameRequest): String? {
        synchronized(this) {
            return if (!started && !done && players.size < MAX_PLAYERS) {
                val uid = UUID.randomUUID().toString();
                players[socketID] = PlayerFactory()
                        .withUsername(req.username)
                        .withColor(req.color)
                        .withPosition(Vec2f(40f * players.size, 0f)) // TODO: generate position based on join index
                        .withUid(uid)
                        .build()
                logger.info("{} joined room", socketID)
                uid
            } else {
                logger.info("{} failed to join room", socketID)
                null
            }
        }
    }

    fun leave(socketID: String): Boolean {
        synchronized(this) {
            return if (!started) {
                if (!players.containsKey(socketID)) {
                    false
                } else {
                    players.remove(socketID)
                    emitPlayers()
                    logger.info("{} left room (before start)", socketID)
                    true
                }
            } else {
                // If the game has started, ignore the leave request; it'll be an implicit loss instead.
                // TODO: if everyone actually has to cross the finish line, record the fact that the user
                // quit to make sure the game knows when it can finish; alternatively, add a timer.
                val player = players[socketID]
                val pc = player!!.getComponent(PlayerComponent::class.java)!!
                pc.finishPosition = -1
                logger.info("{} registered as DNF", socketID)

                false
            }
        }
    }

    fun hasFreeSlots(): Boolean {
        synchronized(this) {
            return players.size < MAX_PLAYERS
        }
    }

    // Player interface {{{
    fun jump(socketId: String) {
        synchronized(this) {
            if (!started) {
                return
            }

            if (!players.containsKey(socketId)) {
                return
            }

            // TODO: perform jump
            players[socketId]!!.getComponent(VelocityComponent::class.java)!!.let {
                if (it.velocity.y == 0f){
                    it.velocity.y = PLAYER_JUMP_VELOCITY
                }
            }
        }
    }


    // }}}

    override fun toString(): String {
        return "[GameRoom, ID=$socketRoomID]"
    }

    companion object {
        const val MAX_PLAYERS = 10
        const val MIN_PLAYERS = 2

        /**
         * The max time (in seconds) to wait after starting to play, given the MIN_PLAYERS threshold
         * has been reached
         *
         * Can also be set via netrunner.max-wait-time for debug purposes.
         */
        val MAX_WAIT_TIME = System.getProperty("netrunner.max-wait-time", "30").toInt()

        const val STANDARD_FIELD_LENGTH = 30_000

        //Try smaller jump velocity later
        const val PLAYER_JUMP_VELOCITY = 1000f
        const val PLAYER_RUN_VELOCITY = 800f

    }
}
