package tdt4240.netrunner.server

import kotlin.jvm.JvmStatic

// Please note that on macOS your application needs to be started with the -XstartOnFirstThread JVM argument

object Main {
    @JvmStatic
    fun main(arg: Array<String>) {
        // This class does as little as possible, and just starts the server
        val server = ServerLauncher()
        server.start()
    }
}
