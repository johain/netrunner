package tdt4240.netrunner.server

import io.socket.engineio.server.EngineIoServer
import io.socket.engineio.server.EngineIoServerOptions
import io.socket.engineio.server.JettyWebSocketHandler
import io.socket.socketio.server.SocketIoServer
import io.socket.socketio.server.SocketIoServerOptions
import io.socket.socketio.server.SocketIoSocket
import org.eclipse.jetty.http.pathmap.ServletPathSpec
import org.eclipse.jetty.server.Handler
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.server.handler.HandlerList
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.websocket.server.WebSocketUpgradeFilter
import org.slf4j.LoggerFactory
import tdt4240.netrunner.server.controllers.GameServerEndpointController
import java.net.InetSocketAddress
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletRequestWrapper
import javax.servlet.http.HttpServletResponse

/**
 * The main server class. This is a helper class that wraps all the complex server logic that the socket.io
 * server library opted not to include for whatever reasons.
 *
 * @param port      The port to listen to. Defaults to 5000, though this is primarily recommended for
 *                  production use. The port should be set to a different value for tests.
 */
class ServerLauncher(val port: Int = System.getProperty("netrunner.server-port", "5000").toInt()) {
    /**
     * The server itself.
     */
    // No. Just no. No one likes this, IntelliJ
    @Suppress("MemberVisibilityCanBePrivate")
    val jettyServer = Server(InetSocketAddress("127.0.0.1", port)).apply {
        connectors = arrayOf(
                ServerConnector(this).also {
                    it.port = port
                    it.reuseAddress = true
                }
        )
    }

    @Suppress("MemberVisibilityCanBePrivate")
    val engineIO = EngineIoServer(EngineIoServerOptions.newFromDefault().apply {
        // Placeholder for potential later use
    })
    @Suppress("MemberVisibilityCanBePrivate")
    val socketIO = SocketIoServer(engineIO, SocketIoServerOptions.newFromDefault().apply {
        // Placeholder for potential later use
    })

    /**
     * The controller for authentication-related processes
     */
    val controllers = listOf(
        GameServerEndpointController(socketIO),
    )

    init {
        logger.info("Port set to {}", port)
        // Server config {{{
        // Registering socket.io is done via a servlet, which requires overriding a bunch of this stuff
        val servletHandler = ServletContextHandler(ServletContextHandler.SESSIONS)
        servletHandler.contextPath = "/"

        // This adds the socket.io bit itself
        servletHandler.addServlet(ServletHolder(object : HttpServlet() {
            override fun service(req: HttpServletRequest, res: HttpServletResponse) {
                // Calls to the servlet are forwarded to engineIO, which is designed to handle this type
                // of processing
                engineIO.handleRequest(object : HttpServletRequestWrapper(req) {
                    override fun isAsyncSupported(): Boolean {
                        return false
                    }
                }, res)
            }
        }), "/socket.io/*")

        // This is deprecated and needs to be fixed, but it works, the alternatives don't, and the
        // server is about as solid as glass near an elephant
        val webSocketUpgradeFilter: WebSocketUpgradeFilter = WebSocketUpgradeFilter.configureContext(servletHandler)
        webSocketUpgradeFilter.addMapping(ServletPathSpec("/socket.io/*")) { _, _ ->
            JettyWebSocketHandler(engineIO)
        }

        val handlerList = HandlerList()
        handlerList.handlers = arrayOf<Handler>(servletHandler)
        jettyServer.handler = handlerList
        // }}}
        // Our stuff
        socketIO.namespace("/").apply {
            // Everything related to the client goes inside an on("connection") (or similar) and operates
            // on a socket rather than a connection.
            on("connection") { data ->
                // Attempt to parse the client
                val client = data.firstOrNull() as? SocketIoSocket
                logger.info("Received connection from {} (ID: {})", "UNK:NO IP SUPPORT", client?.id ?: "null?")

                // The client should never be null in practice, but if it is, this ensures nothing is executed.
                // The socket.io-server-java docs are also awful when it comes to nullability here.
                client?.let {
                    for (controller in controllers) {
                        controller.register(it)
                    }
                }
            }
            on("error") { exceptStack ->
                for (err in exceptStack) {
                    if (err is Exception) {
                        err.printStackTrace()
                    } else {
                        System.err.println(err)
                    }
                }
            }
        }

        logger.info("Server is ready")
    }

    fun start() {
        jettyServer.start()
    }

    fun stop() {
        jettyServer.stop()
        getController<GameServerEndpointController>().kill()
    }

    /**
     * This is a nasty hack; do not use outside tests
     */
    internal inline fun <reified T> getController(): T {
        for (controller in controllers) {
            if (controller is T) {
                return controller
            }
        }
        throw RuntimeException("Failed to find controller of matching type")
    }

    internal fun getClientsInRoom(room: String) = socketIO.namespace("/").adapter.listClients(room)

    companion object {
        private val logger = LoggerFactory.getLogger(ServerLauncher::class.java)

        init {
            System.setProperty("org.eclipse.jetty.util.log.announce", "false")
        }
    }
}
