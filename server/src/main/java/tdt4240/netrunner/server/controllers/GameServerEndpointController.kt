package tdt4240.netrunner.server.controllers

import io.socket.socketio.server.SocketIoServer
import io.socket.socketio.server.SocketIoSocket
import org.slf4j.LoggerFactory
import tdt4240.netrunner.model.requests.JoinGameRequest
import tdt4240.netrunner.model.response.StatusCode
import tdt4240.netrunner.model.response.StatusResponse
import tdt4240.netrunner.model.util.gsonImpl
import tdt4240.netrunner.server.game.GameRoom
import tdt4240.netrunner.server.util.SocketIOInputNormaliser
import java.security.MessageDigest
import java.util.Random
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedDeque

class GameServerEndpointController(val server: SocketIoServer) : ServerFeatureController() {
    /**
     * Used for tracking all games - both ongoing and queued
     */
    val games = ConcurrentHashMap<String, GameRoom>()

    /**
     * Used for matchmaking purposes: contains queued games. All games present in this map
     * must also be present in {@link GameController#games}
     */
    val playerGameMap = ConcurrentHashMap<String, String>()
    val queuedGames = ConcurrentLinkedDeque<GameRoom>()
    var running = true

    // Absolute overkill, but might as well
    private val hasher = MessageDigest.getInstance("SHA3-256")

    override fun register(socket: SocketIoSocket) {

        socket.on("join-matchmaking") {
            val (joinReq, ack) = SocketIOInputNormaliser.normaliseSingleWithAck<JoinGameRequest>(it)
                    ?: return@on
            try {
                if (ack == null) {
                    // No ack, no game. It's the law
                    return@on
                }
                val uid = joinGame(socket, joinReq)
                if (uid != null) {
                    ack.sendAcknowledgement(gsonImpl.toJson(StatusResponse(StatusCode.OK, uid)))
                } else {
                    ack.sendAcknowledgement(gsonImpl.toJson(StatusResponse(StatusCode.FAIL, "Failed to join game. Try again.")))
                }
            } catch (e: Throwable) {
                ack?.sendAcknowledgement(gsonImpl.toJson(StatusResponse(StatusCode.FAIL, "An internal server error occurred. Try again later")))
                e.printStackTrace();
                throw e;
            }
        }
        socket.on("leave-matchmaking") {
            try {
                logger.info("Player {} requested to leave", socket.id)
                val ack = SocketIOInputNormaliser.normaliseAck(it)
                leaveGame(socket)
                // There's no real reason to not acknowledge a leave request as complete. At least
                // not for now.
                ack?.sendAcknowledgement(gsonImpl.toJson(StatusResponse(StatusCode.OK)))
            } catch (e: Throwable) {
                e.printStackTrace();
                throw e;
            }
        }
        socket.on("jump") {
            try {
                val ack = SocketIOInputNormaliser.normaliseAck(it)

                val roomId = playerGameMap[socket.id]
                if (roomId == null) {
                    // I'm increasingly growing uncertain that acks are the way to go. It's a bit of a callback hell
                    // and the risk it gets worse is _very_ real.
                    // But like, what's the alternative? `socket.emit("error", someObj)` isn't much better
                    // Might just have to be accepted for now.
                    ack?.sendAcknowledgement(gsonImpl.toJson(StatusResponse(StatusCode.FAIL, "That's a shady move :squint:")))
                } else {
                    val room = games[roomId]
                    if (room == null) {
                        ack?.sendAcknowledgement(gsonImpl.toJson(StatusResponse(StatusCode.FAIL, "You're not in a game")))
                        return@on
                    }

                    room.jump(socket.id)
                }
            } catch (e: Throwable) {
                e.printStackTrace();
                throw e;
            }
        }
        socket.on("disconnect") {
            try {
                notifyPlayerQuit(socket.id)
            } catch (e: Throwable) {
                e.printStackTrace();
                throw e;
            }
        }

    }

    fun kill() {
        running = false
        for ((_, room) in this.games) {
            room.kill()
        }
    }

    private fun joinGame(socket: SocketIoSocket, req: JoinGameRequest): String? {
        // Accidental multi-join avoidance
        leaveGame(socket)
        val game: GameRoom = queuedGames.firstOrNull {
            it.hasFreeSlots()
        } ?: let {
            val room = GameRoom(generateGameID(), server, this)
            queuedGames.add(room)
            games[room.socketRoomID] = room

            room
        }

        // On today's episode of stupid behavior... socket.io's room implementation (in Java anyway)
        // isn't thread-safe! I wonder who thought that was a good idea, but it means our code needs
        // to implement a double safety on join()
        val res: String?
        synchronized(this) {
            res = game.join(socket.id, req)
        }
        // Make sure the game was joined before actually registering anything
        // This will only be an edge-case for games that have just started, or just filled up.
        if (res != null) {
            socket.joinRoom(game.socketRoomID)
            game.emitPlayers()

            playerGameMap[socket.id] = game.socketRoomID
            logger.info("Player {} (username: {}) joined room {}", socket.id, req.username, game.socketRoomID)
        }
        return res
    }

    private fun leaveGame(socket: SocketIoSocket) {
        playerGameMap[socket.id]?.let { gameRoomId ->
            playerGameMap.remove(socket.id)

            games[gameRoomId]?.let { room ->
                synchronized(this) {
                    socket.leaveRoom(gameRoomId)
                }
                room.leave(socket.id)
            }
        }
    }

    private fun generateGameID() : String {
        var id: String
        do {
            val hash = hasher.digest(
                    Random().nextLong().toString().encodeToByteArray()
            )
            // The rest of the digest are bytes that need to be decoded into their hex form to
            // look like a standard hash
            val decoded: StringBuilder = StringBuilder(2 * hash.size)
            for (i in hash.indices) {
                val hex = Integer.toHexString(0xff and hash[i].toInt())
                if (hex.length == 1) {
                    decoded.append('0')
                }
                decoded.append(hex)
            }
            id = decoded.toString()
        } while (games.containsKey(id))
        return id;
    }

    fun notifyStart(room: GameRoom) {
        logger.info("Room ${room.socketRoomID} notified start")
        queuedGames.remove(room)
    }

    fun notifyDone(room: GameRoom) {
        logger.info("Room ${room.socketRoomID} notified done")
        games.remove(room.socketRoomID)
        // Concurrency protection
        if (queuedGames.contains(room)) {
            queuedGames.remove(room)
        }

        for ((socketID, unused) in room.players) {
            playerGameMap.remove(socketID)
        }
        // DC all clients form the room
        server.namespace("/").adapter.listClients(room.socketRoomID)
                .forEach {
                    it.leaveRoom(room.socketRoomID)
                }
    }

    fun notifyPlayerQuit(socketID: String) {
        if (playerGameMap.containsKey(socketID)) {
            games[playerGameMap[socketID]]?.leave(socketID)
            playerGameMap.remove(socketID)
        }
    }

    fun notifyDead(room: GameRoom) {
        logger.info("Room {} is dead and will be disposed", room)
        games.remove(room.socketRoomID)
        queuedGames.remove(room)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(GameServerEndpointController::class.java)
    }
}
