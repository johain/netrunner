package tdt4240.netrunner.server.controllers

import io.socket.socketio.server.SocketIoSocket

/**
 * Helper class for dealing with server features. This is primarily to better allow for scoping
 * and relevant context to be preserved, rather than dumping every single `socket.on()` into
 * ServerLauncher.kt.
 */
abstract class ServerFeatureController {

    /**
     * This function registers relevant events onto a provided SocketIoSocket.
     *
     * @param socket        The socket to register events with.
     */
    abstract fun register(socket: SocketIoSocket)
}
