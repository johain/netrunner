package tdt4240.netrunner.server.util

import com.google.gson.Gson
import io.socket.socketio.server.SocketIoSocket.ReceivedByLocalAcknowledgementCallback
import tdt4240.netrunner.model.response.StatusCode
import tdt4240.netrunner.model.response.StatusResponse
import tdt4240.netrunner.model.util.gsonImpl

object SocketIOInputNormaliser {
    /**
     * Helper function for extracting the ack parameter
     */
    fun normaliseAck(
            input: Array<out Any>
    ): ReceivedByLocalAcknowledgementCallback? {
        if (input.isEmpty()) return null
        return input[input.size - 1] as? ReceivedByLocalAcknowledgementCallback
    }

    /**
     * Helper function for parsing a single argument, along with the ack.
     *
     * Note that this function only parses the first argument. If there's a reason to add multiple
     * arguments, do reconsider and check if it can be done with data objects instead.
     */
    inline fun <reified T> normaliseSingleWithAck(
            input: Array<out Any>
    ): Pair<T, ReceivedByLocalAcknowledgementCallback?>? {
        val ack = input[input.size - 1] as? ReceivedByLocalAcknowledgementCallback
        if (input.size == 1) {
            // I'm 99% sure the ack is always present, so this shouldn't cause problems.
            //
            // hi, past me. You were wrong, the ack is not always present, because reasons? I don't know,
            // it's an awfully designed library.
            //
            // Pain
            ack?.sendAcknowledgement(Gson().toJson(StatusResponse(StatusCode.INSUFFICIENT_ARGUMENTS,
                    "Developer error: insufficient arguments passed to endpoint")))
            return null
        }

        // TODO: try-catch with error handling
        val res = gsonImpl.fromJson(input[0] as String, T::class.java)
        if (res == null) {
            ack?.sendAcknowledgement(Gson().toJson(StatusResponse(StatusCode.DATA_ERROR, "Received null or incorrect data")))
            return null
        }

        return Pair(res, ack)
    }
}