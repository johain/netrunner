package tdt4240.netrunner.game

import tdt4240.netrunner.model.game.Entity
import tdt4240.netrunner.model.game.components.level.FinishLineComponent
import tdt4240.netrunner.model.game.components.level.GroundComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.game.factories.FinishLineFactory
import tdt4240.netrunner.model.game.factories.GroundFactory
import tdt4240.netrunner.model.game.factories.PlatformFactory
import tdt4240.netrunner.model.game.factories.PowerupFactory
import tdt4240.netrunner.model.util.Vec2f
import tdt4240.netrunner.server.game.GameRoom
import java.util.*
import kotlin.math.roundToInt

class WorldGen(val room: GameRoom) {
    fun genWorld(length: Int) : List<Entity> {

        val entities = mutableListOf<Entity>()

        val randSpeed = 0.5f
        val random = Random()
        fun rand(from: Int, to: Int) : Int {
            return random.nextInt(to - from) + from
        }
        val randNum = rand(0,1)
        if(randNum == 0){
            val randSpeed = 1.5f
        }

        for (i in -3..((length + 8 * GroundComponent.MAX_WIDTH) / GroundComponent.MAX_WIDTH).roundToInt() + 2) {
            entities.add(GroundFactory()
                    .withStartPos(Vec2f(i * GroundComponent.MAX_WIDTH, BASE_HEIGHT))
                    .withEndPos(Vec2f((i + 1) * GroundComponent.MAX_WIDTH, BASE_HEIGHT))
                    .build()
            )

            //entities.add(PowerupFactory()
            //  .withPos(entities[entities.size-1].getComponent(GroundComponent::class.java)!!.startPos)
            //.withSpeedModifier(randSpeed)        //TODO fix random speed
            //.build()
            //)
        }

        entities.add(FinishLineFactory()
                .withPos(Vec2f(length.toFloat(), BASE_HEIGHT))
                .build()
        )

        var x = 1500f

        while (x <= length) {
            val width = random.nextInt(PlayerComponent.PLAYER_WIDTH.roundToInt() * 6) + PlayerComponent.PLAYER_WIDTH.roundToInt() * 2
            entities.add(PlatformFactory()
                    .withLeftPos(Vec2f(x, BASE_HEIGHT + (random.nextInt(150) + 50).toFloat()))
                    .withWidth(width)
                    .build()
            )

            // A uniform distribution isn't _great_, but Java doesn't offer that many facilities
            // for other distribution types.
            // It can be implemented manually (https://stackoverflow.com/a/5969719/6296561), but
            // but it isn't nearly as convenient as std::normal_distribution
            // (https://en.cppreference.com/w/cpp/numeric/random/normal_distribution)
            // C++ is exceptional with algorithms though.
            x += width + random.nextInt(550) - 50
        }

        entities.add(PowerupFactory()
                .withPos(Vec2f(1000f, 200f))
                .withSpeedModifier(randSpeed)        //TODO fix random speed
                .build()
        )
        return entities
    }

    companion object {
        const val BASE_HEIGHT = 50f
    }
}
