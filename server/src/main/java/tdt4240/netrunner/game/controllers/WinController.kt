package tdt4240.netrunner.game.controllers

import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.level.FinishLineComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.game.hitboxes.Rectangle
import tdt4240.netrunner.server.game.GameRoom
import kotlin.math.max

class WinController(ecs: EcsEngine) : EcsController(ecs) {
    override fun tick(delta: Double) {
        super.tick(delta)

        val players = super.ecs.getEntitiesByComponent(PlayerComponent::class.java)
        val finishLine = super.ecs.getEntitiesByComponent(FinishLineComponent::class.java)

        var currMaxPos = players.map {
            it.getComponent(PlayerComponent::class.java)!!.finishPosition
        }.max()
        if (currMaxPos < 0) currMaxPos = 0

        for (player in players) {
            val pos = player.getComponent(PositionComponent::class.java)!!
            val dims = player.getComponent(DimensionComponent::class.java)!!
            val playerComp = player.getComponent(PlayerComponent::class.java)!!
            if (playerComp.finishPosition == 0) {
                val bb = Rectangle(pos.pos, dims.dims)
                for (line in finishLine) {
                    val finishPos = line.getComponent(PositionComponent::class.java)!!
                    val finishDims = line.getComponent(DimensionComponent::class.java)!!

                    val finishBB = Rectangle(finishPos.pos, finishDims.dims)
                    if (finishBB.overlaps(bb)) {
                        playerComp.finishPosition = ++currMaxPos
                        break
                    }
                }
            } else {
                player.getComponent(VelocityComponent::class.java)!!.apply {
                    // This sounded like a good idea when I wrote it, but it really isn't.
                    // This means the winner can end up floating in the air xd
                    // velocity.y = 0f
                    if (velocity.x > 0f) {
                        // The max ensures the velocity never goes below 0, which would be stupid.
                        velocity.x = max(velocity.x - GAME_END_BREAK * delta.toFloat(), 0f)
                    }
                }

            }
        }
    }

    companion object {
        const val GAME_END_BREAK = GameRoom.PLAYER_RUN_VELOCITY / 2f
    }
}