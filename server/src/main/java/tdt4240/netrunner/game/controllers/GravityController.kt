package tdt4240.netrunner.game.controllers

import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.Entity
import tdt4240.netrunner.model.game.components.dynamic.GravityComponent
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityModifier
import tdt4240.netrunner.model.game.components.dynamic.VelocityModifierComponent
import tdt4240.netrunner.model.game.components.level.GroundComponent
import tdt4240.netrunner.model.game.components.level.PlatformComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.game.hitboxes.Rectangle

class GravityController(ecs: EcsEngine) : EcsController(ecs) {

    private fun setPlatformSpeedBoost(player: Entity, vel: VelocityComponent, enable: Boolean) {
        val vmc = player.getComponent(VelocityModifierComponent::class.java)!!

        if (!vmc.mods.containsKey(MOD_PSB)) {
            vmc.mods[MOD_PSB] = mutableListOf()
        }

        if (enable) {
            if (vmc.mods[MOD_PSB]!!.isNotEmpty()) {
                // Done
                return
            }
            vmc.mods[MOD_PSB]!!.add(
                    VelocityModifier(PSB_BOOST)
            )
            vel.velocity.x *= PSB_BOOST

        } else {
            if (vmc.mods[MOD_PSB]!!.isEmpty()) {
                // Done
                return
            }
            val boost = vmc.mods[MOD_PSB]!![0].speedModifier
            vel.velocity.x /= boost

            vmc.mods[MOD_PSB]!!.clear()

        }

    }

    override fun tick(delta: Double) {
        outer@for (entity in super.ecs.entities) {
            entity.getComponent(PositionComponent::class.java)?.let { pos ->
                entity.getComponent(VelocityComponent::class.java)?.let { vel ->
                    entity.getComponent(DimensionComponent::class.java)?.let { dims ->
                        val bb = Rectangle(pos.pos, dims.dims)
                        val platforms = super.ecs.getEntitiesByComponent(PlatformComponent::class.java)

                        // It's possible for there to not be any platforms immediately near the player.
                        if (platforms.isNotEmpty()) {
                            for (platform in platforms) {

                                val pPos = platform.getComponent(PositionComponent::class.java)!!
                                val pComp = platform.getComponent(PlatformComponent::class.java)!!
                                if (pos.pos.x >= pPos.pos.x && pos.pos.x <= pPos.pos.x + pComp.width) {
                                    val pDims = platform.getComponent(DimensionComponent::class.java)!!
                                    val platformBB = Rectangle(pPos.pos, pDims.dims)

                                    if (bb.overlaps(platformBB)) {
                                        if (pPos.pos.y < pos.pos.y && vel.velocity.y <= 0) {
                                            vel.velocity.y = 0f
                                            pos.pos.y = pPos.pos.y + pDims.dims.y
                                            // continue @outer isn't allowed for some reason, so even though this is a name clash,
                                            // it'll have to be fine. It stops execution inside the inner let regardless of where it goes
                                            setPlatformSpeedBoost(entity, vel, true)
                                            return@let
                                        }
                                    }
                                }
                            }
                        }
                        setPlatformSpeedBoost(entity, vel, false)
                        // Check ground collisions {{{
                        val groundComponent = super.ecs.getEntitiesByComponent(GroundComponent::class.java)
                                .map {
                                    it.getComponent(GroundComponent::class.java)!!
                                }.firstOrNull { component ->
                                    pos.pos >= component.startPos && pos.pos < component.endPos
                                }

                        val groundY = groundComponent?.yAt(pos.pos.x)
                        if (groundY != null && pos.pos.y <= groundY) {
                            pos.pos.y = groundY
                            vel.velocity.y = 0f
                        } else {
                            entity.getComponent(GravityComponent::class.java)?.let { gravity ->
                                // Update gravity acceleration
                                vel.velocity.plusAssign(gravity.gravSpeed * delta)
                            }
                        }
                        // }}}

                    }
                }
            }
        }
    }

    companion object {
        val MOD_PSB = "PlatformSpeedBoost"
        const val PSB_BOOST = 1.25f
    }
}