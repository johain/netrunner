package tdt4240.netrunner.game.controllers

import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.living.DimensionComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.game.components.powerups.PowerupComponent
import tdt4240.netrunner.model.game.hitboxes.Rectangle

class powerupSpeedController(ecs: EcsEngine) : EcsController(ecs) {
    override fun tick(delta: Double) {

        super.tick(delta)

        val players = super.ecs.getEntitiesByComponent(PlayerComponent::class.java)
        val powerups = super.ecs.getEntitiesByComponent(PowerupComponent::class.java)

        for (player in players){
            for (powerup in powerups){
                val powerupComponent = powerup.getComponent(PowerupComponent::class.java)!!
                if(powerupComponent.consumed){
                    continue
                }
                player.getComponent(PositionComponent::class.java)!!.also { pos ->
                    val dims = player.getComponent(DimensionComponent::class.java)!!
                    val bb = Rectangle(pos.pos, dims.dims)
                    powerup.getComponent(PositionComponent::class.java)!!.also { powerupPos ->
                        powerup.getComponent(DimensionComponent::class.java)!!.also { powerupDims ->
                            val powerupBB = Rectangle(powerupPos.pos, powerupDims.dims)

                            powerupComponent.consumed = false
                            if (bb.overlaps(powerupBB)) {
                                val playerVelocity = player.getComponent(VelocityComponent::class.java)!!

                                powerupComponent.consumed = true
                                playerVelocity.velocity *= powerupComponent.speedModifier
                                //TODO add timeout functionality
                            }
                        }

                    }
                }
            }
        }
        super.ecs.entities.removeIf {entity->
            entity.getComponent(PowerupComponent::class.java)?.consumed ?: false
        }
    }
}
