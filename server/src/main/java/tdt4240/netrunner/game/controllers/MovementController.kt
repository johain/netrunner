package tdt4240.netrunner.game.controllers

import tdt4240.netrunner.model.game.EcsController
import tdt4240.netrunner.model.game.EcsEngine
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent

class MovementController(ecs: EcsEngine) : EcsController(ecs) {
    override fun tick(delta: Double) {
        for (entity in super.ecs.entities) {
            entity.getComponent(PositionComponent::class.java)?.let { pos ->
                entity.getComponent(VelocityComponent::class.java)?.let { vel ->
                    if (entity.getComponent(PlayerComponent::class.java)?.finishPosition ?: 0 >= 0) {
                        // This controller is deceptively simple: just add the velocity
                        // to the position.
                        // Poof, controller done.
                        // A win check should be performed here for players though, but that's blocked
                        // by #26 om the task doc, and so cannot be implemented yet.
                        pos.pos.plusAssign(vel.velocity * delta)
                    }
                }
            }
        }
   }
}