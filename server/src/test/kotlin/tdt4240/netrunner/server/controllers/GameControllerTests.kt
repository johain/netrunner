package tdt4240.netrunner.server.controllers

import io.socket.client.Socket
import org.junit.jupiter.api.TestInstance
import tdt4240.netrunner.model.response.StatusCode
import tdt4240.netrunner.server.ServerLauncher
import tdt4240.netrunner.server.game.GameRoom
import tdt4240.netrunner.server.util.*
import tdt4240.netrunner.server.util.GameUtil.addPlayer
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.test.*

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class GameControllerTests {
    private val server = ServerLauncher(60009)
    private val gameController: GameServerEndpointController = server.getController()

    private lateinit var client: Socket

    @BeforeTest
    fun initServer() {
        server.start()

        server.engineIO.on("error") {
            fail((it[0] as Exception).message)
        }

        /**
         * The Java client and server for socket.io has to be some of the worst software I've ever seen.
         * There's a static Thread in EventThread, but that thread is recreated constantly.
         * The ThreadFactory doesn't allow for any modifications, and thanks to Java 18 (even though the standard
         * used is Java 8), there's no way to un-final a field.
         *
         * To add insult to injury, there's also no trivial way to mock anything non-destructively, which means
         * there's no way to consistently ensure the created Thread throws properly.
         *
         * The Executor used by Socket.io swallows exceptions as well, and only reflects it in a logger.
         * Why they even bother to re-throw the exception is beyond me. There's no way to modify its behavior
         * anyway, and seeing as they log it, throwing the exception does a grand total of absolutely
         * nothing.
         *
         * TL;DR: Java's Executors.newSingleThreadExecutor() swallows exceptions, Socket.io's bad design
         * prevents that behavior from being changed naturally, and updates to Java prevents attempts
         * from forcibly changing the behaviour of the system.
         *
         * This means that the actual exception is only present as a part of the console output, which means
         * the tests have to be executed with `--info` to ensure JUnit doesn't suppress the output.
         * It also means the full logs have to be read to figure out the actual source of the problem,
         * as the tests here generally result in a generic "failed to return in time", which isn't the actual
         * problem most of the time.
         *
         * I haven't even tried to get the server to throw yet, but on the assumption it's as bad as the client
         * (which appears to be the case; it is suppressing error messages), it isn't worth trying to get it to
         * cooperate.
         *
         * Basically... it's not possible to test properly. There were attempts, and those attempts failed, and
         * were exhaustive for the available options. It's broken and can't be fixed without changing the library,
         * which I simply don't have time to even begin to attempt with the limited time available. If this
         * was an actual project that would go into production, I would definitely open an issue and ask for
         * a way to handle errors better, but there isn't enough time. Especially with the disproportionate amount
         * of time sunk into this mess. Enough is enough.
         *
         * (I really miss C++ right now.)
         */
        client = initSocket(server.port)
    }

    @Test
    fun testSingleClientJoin() {
        val control = AtomicBoolean(false)
        addPlayer(client, "JoeMama", 1)

        assertEquals(1, gameController.queuedGames.size)
        assertEquals(1, gameController.playerGameMap.size)
        assertEquals(1, gameController.games.size)

        val game = gameController.queuedGames.first


        assertEquals(1, server.getClientsInRoom(game.socketRoomID).size)
        assertTrue(game.thread.isAlive)

        control.set(false)
        client.leaveMatch { res ->
            assertEquals(StatusCode.OK, res.status)
            control.set(true)
        }
        AsyncAssert.waitFor(true, property = control::get)

        assertEquals(0, server.getClientsInRoom(game.socketRoomID).size)

        // Leaving doesn't axe the game, but it does axe the entry from the playerGameMap.
        // The game may still expire, but at the time of writing, that isn't implemented,
        // and will be tested separately when it is implemented anyway.
        assertEquals(1, gameController.queuedGames.size)
        assertEquals(0, gameController.playerGameMap.size)
        assertEquals(1, gameController.games.size)

    }

    @Test
    fun testMultipleClients() {

        addPlayer(client, "JoeMama", 1)

        val altSocket = initSocket(server.port)

        addPlayer(altSocket, "PickleRick", 2)

        assertEquals(1, gameController.queuedGames.size)
        assertEquals(2, gameController.playerGameMap.size)
        assertEquals(1, gameController.games.size)
    }

    @Test
    fun testMaxClients() {
        val sockets = mutableListOf<Socket>()
        for (i in 0 until GameRoom.Companion.MAX_PLAYERS) {
            sockets.add(initSocket(server.port))

            addPlayer(sockets[sockets.size - 1], "PickleRick$i", i + 1)
        }

        // TODO: change the first one here to 0 when game setup has been properly implemented.
        assertEquals(1, gameController.queuedGames.size)
        assertEquals(10, gameController.playerGameMap.size)
        assertEquals(1, gameController.games.size)
        assertTrue(gameController.queuedGames.elementAt(0).players.size == 10)

        sockets.add(initSocket(server.port))
        addPlayer(sockets[sockets.size - 1], "PickleRick11", 1)


        assertEquals(1, gameController.queuedGames.size)
        assertEquals(11, gameController.playerGameMap.size)
        assertEquals(2, gameController.games.size)

        sockets.forEach {
            it.close()
        }
    }

    @AfterTest
    fun stopServer() {
        server.stop()
    }

    companion object {
        init {
            System.setProperty("netrunner.max-wait-time", "60")
        }
    }

}
