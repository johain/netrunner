package tdt4240.netrunner.server.controllers.gamesim

import org.slf4j.LoggerFactory
import tdt4240.netrunner.model.game.components.dynamic.PositionComponent
import tdt4240.netrunner.model.game.components.dynamic.VelocityComponent
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.server.ServerLauncher
import tdt4240.netrunner.server.controllers.GameServerEndpointController
import tdt4240.netrunner.server.game.GameRoom
import tdt4240.netrunner.server.util.AsyncAssert
import tdt4240.netrunner.server.util.initSocket
import tdt4240.netrunner.server.util.joinForUid
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue
import kotlin.test.fail

class ValidateGameFlowTests {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val server = ServerLauncher(60009)
    private val gameController: GameServerEndpointController = server.getController()

    @BeforeTest
    fun initServer() {
        server.start()

        server.engineIO.on("error") {
            fail((it[0] as Exception).message)
        }
    }

    @Test
    fun testJump() {
        val control = AtomicBoolean(false)
        val c1 = initSocket(server.port)
        val c2 = initSocket(server.port)

        val p1uid = c1.joinForUid("JoeMama")
        val p2uid = c2.joinForUid("PickleRick")

        val room = gameController.queuedGames.elementAt(0)
        room.forceStart = true

        // wait for the game to actually start
        control.set(false)
        c1.once("game-started") {
            control.set(true)
        }
        AsyncAssert.waitFor(true, sleep = 1000, property = control::get,
            message = "If this throws, the game-started event was not received")

        assertEquals(0, gameController.queuedGames.size)
        assertEquals(2, gameController.playerGameMap.size)
        assertEquals(1, gameController.games.size)

        control.set(false)
        c1.emit("jump")

        Thread.sleep(100)
        val players = room.ecs.getEntitiesByComponent(PlayerComponent::class.java)
        for (player in players) {
            val pc = player.getComponent(PlayerComponent::class.java)
            val pv = player.getComponent(VelocityComponent::class.java)

            assertNotNull(pc)
            assertNotNull(pv)

            if (pc.uid == p1uid) {
                // Player 1 has jumped, and should be registered as such
                assertTrue(pv.velocity.y > 0f)
            } else {
                // Player 2 hasn't, and should be on the ground.
                assertTrue(pv.velocity.y == 0f)
            }
            // Both should be moving in the x direction.
            //
            // Doesn't really make sense to test it in an entire separate test, so might as well
            // do it here.
            assertTrue(pv.velocity.x > 0f)
        }
    }

    @Test
    fun validateGameWin() {
        val control = AtomicBoolean(false)
        val c1 = initSocket(server.port)
        val c2 = initSocket(server.port)

        val p1uid = c1.joinForUid("JoeMama")
        val p2uid = c2.joinForUid("PickleRick")

        val room = gameController.queuedGames.elementAt(0)
        room.forceStart = true

        // wait for the game to actually start
        control.set(false)
        c1.once("game-started") {
            control.set(true)
        }
        AsyncAssert.waitFor(true, sleep = 1000, property = control::get,
                message = "If this throws, the game-started event was not received")

        control.set(false)

        val players = room.ecs.getEntitiesByComponent(PlayerComponent::class.java)
        for (player in players) {
            val pc = player.getComponent(PlayerComponent::class.java)
            val pos = player.getComponent(PositionComponent::class.java)

            assertNotNull(pc)
            assertNotNull(pos)

            if (pc.uid == p1uid) {
                // Give p1 a second worth of advantage
                pos.pos.x = GameRoom.STANDARD_FIELD_LENGTH.toFloat() - 1600f
            } else {
                // Move p2 right behind for leaderboard testing
                pos.pos.x = GameRoom.STANDARD_FIELD_LENGTH.toFloat() - 2400f
            }
            assertEquals(0, pc.finishPosition)
        }

        val p2 = players.map {
            it.getComponent(PlayerComponent::class.java)!!
        }.first {
            it.uid == p2uid
        }

        AsyncAssert.waitFor(true, sleep = 1000, property = {
            p2.finishPosition != 0
        }, message = "If this throws, the server was most likely unable to finish game processing in a sane amount of time")

        for (player in players) {
            val pc = player.getComponent(PlayerComponent::class.java)

            assertNotNull(pc)

            if (pc.uid == p1uid) {
                assertEquals(1,pc.finishPosition)
            } else {
                assertEquals(2, pc.finishPosition)
            }
        }


    }

    @AfterTest
    fun stopServer() {
        server.stop()
    }

}