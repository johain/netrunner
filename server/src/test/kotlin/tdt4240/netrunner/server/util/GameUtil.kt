package tdt4240.netrunner.server.util

import io.socket.client.Socket
import org.junit.jupiter.api.Assertions.assertTrue
import tdt4240.netrunner.model.game.components.living.PlayerComponent
import tdt4240.netrunner.model.response.EntityData
import tdt4240.netrunner.model.response.StatusCode
import tdt4240.netrunner.model.util.gsonImpl
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

object GameUtil {
    fun addPlayer(client: Socket, user: String, expectedPlayerCount: Int) {
        val control = AtomicBoolean(false)
        val receivedPlayers = AtomicBoolean(false)
        client.once("entities") { entities ->
            // Not entirely clear on why an explicit cast is necessary.
            // It's clearly able to figure it out at compile time, just not in the IDE
            val data = gsonImpl.fromJson(entities[0] as String, EntityData::class.java) as EntityData
            assertEquals(expectedPlayerCount, data.entities.size)
            assertNotNull(data[data.entities.size - 1].getComponent(PlayerComponent::class.java))
            assertTrue(
                    data.entities.any {
                        it.getComponent(PlayerComponent::class.java)!!.username == user
                    }
            )
            receivedPlayers.set(true)
        }

        client.joinMatch(user) { res ->
            assertEquals(StatusCode.OK, res.status, res.message)
            control.set(true)
        }
        AsyncAssert.waitFor(true, property = control::get)
        AsyncAssert.waitFor(true, property = receivedPlayers::get)
    }
}