package tdt4240.netrunner.server.util

import kotlin.test.fail

object AsyncAssert {
    fun <T> waitFor(expected: T, maxCount: Int = 5, sleep: Long = 300, property: () -> T, message: String = "") {
        for (i in 0..maxCount) {
            if (property() == expected) {
                return
            }
            Thread.sleep(sleep)
        }
        fail("Property didn't return expected value in time: " + message.ifEmpty { "(no additional info)" })
    }

}