package tdt4240.netrunner.server.util

import io.socket.client.Ack
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.WebSocket
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tdt4240.netrunner.model.game.data.PlayerColor
import tdt4240.netrunner.model.requests.JoinGameRequest
import tdt4240.netrunner.model.response.StatusCode
import tdt4240.netrunner.model.response.StatusResponse
import tdt4240.netrunner.model.util.gsonImpl
import java.net.URI
import java.util.concurrent.atomic.AtomicBoolean

private val Socket.logger: Logger
    get() = LoggerFactory.getLogger(Socket::class.java)

fun Socket.leaveMatch(also: ((res: StatusResponse) -> Unit)?) {
    emit("leave-matchmaking", Ack {
        val res = gsonImpl.fromJson(it[0] as String, StatusResponse::class.java)
        also?.invoke(res)
    })
}

fun Socket.joinMatch(username: String, also: ((res: StatusResponse) -> Unit)?) {

    emit("join-matchmaking", gsonImpl.toJson(JoinGameRequest(username, PlayerColor.BLUE)), Ack {
        val res = gsonImpl.fromJson(it[0] as String, StatusResponse::class.java)

        also?.invoke(res)
    })
}

fun Socket.joinForUid(username: String): String {
    val control = AtomicBoolean(false)
    lateinit var uid: String

    joinMatch("JoeMama") {
        if (it.status != StatusCode.OK) {
            this.logger.error("Didn't receive StatusCode OK: {}", it.message)
        } else {
            uid = it.message
            control.set(true)
        }
    }
    AsyncAssert.waitFor(true, property = control::get,
            message = "If this throws, the user \"${username}\" likely didn't receive a UID")
    return uid
}

fun initSocket(port: Int): Socket {
    val client = IO.socket(URI("ws://127.0.0.1:$port"), IO.Options.builder()
            // Force websocket transport rather than polling
            .setTransports(arrayOf(WebSocket.NAME))
            .build())
    client.connect()
    AsyncAssert.waitFor(true, property = client::connected)

    return client
}
