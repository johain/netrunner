#!/usr/bin/bash
echo "Note that this script is intended for server use only."
echo "Unless you are the server sysadmin, this probably isn't the script"
echo "you are looking for."
echo "There are no scripts to simplify the dependency install process"
echo "for end-users."
if [ "$EUID" -ne 0 ]; then
  echo "This script requires root access."
  exit
fi

apt update
apt install -y curl unzip zip openjdk-17-jdk-headless

cp etc / -r
